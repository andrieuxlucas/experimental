﻿/**
 *	AerialState.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/08/2018	(dd/mm/yy)
 */

public enum AerialState
{
	Idle, Jumping, Falling, None
}