﻿/**
 *	AllowedRunMode.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	06/03/2019 (dd/mm/yy)
 */

public enum AllowedRunMode
{
	Complete, ForwardAndSideways, ForwardOnly, ForwardPositiveOnly
}