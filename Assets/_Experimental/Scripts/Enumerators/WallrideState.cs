﻿/**
 *	WallrideState.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	24/02/2019 (dd/mm/yy)
 */

public enum WallrideState
{
    Idle, Wallriding
}