﻿namespace Utitlities.Math
{
    public abstract class MathFunction
    {
        public static float operator +(MathFunction func, float f)
        {
            float value = func.Value;
            func.Step(f);

            return value;
        }

        public abstract float Value { get; set; }
        public abstract void Step(float step);
    }
}