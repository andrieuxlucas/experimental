﻿/**
 *	DirectionalScan.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	01/07/2019 (dd/mm/yy)
 */

using UnityEngine;

namespace Utitlities.ScanningMethods
{
    public struct DirectionalConfig
    {
        public DirectionalConfig(LayerMask ScanableLayers, Vector3 InitialDirection,
                                 Vector3 InitialPosition,
                                 float MaxDistance, float ScanRadius, float Speed,
                                 int Precision,
                                 bool DisableOnHit, bool Debug)
        {
            this.InitialDirection = InitialDirection;
            this.InitialPosition = InitialPosition;
            this.ScanableLayers = ScanableLayers;

            this.DisableOnHit = DisableOnHit;
            this.Debug = Debug;

            this.Precision = Precision;

            this.MaxDistance = MaxDistance;
            this.ScanRadius = ScanRadius;
            this.Speed = Speed;
        }

        public LayerMask ScanableLayers;
        public Vector3 InitialDirection;
        public Vector3 InitialPosition;

        public float MaxDistance;
        public float ScanRadius;
        public float Speed;

        public bool DisableOnHit;
        public bool Debug;

        public int Precision;
    }

    /// <summary>
    /// Used to create derectional capsular projections.
    /// </summary>
    public class DirectionalScan
    {
        /// <summary>
        /// The distance from the previous scanned point to the current scanned point.
        /// </summary>
        public float CurrentDistance => Vector3.Distance(CurrentScanInitialPosition, CurrentScanFinalPosition);
        /// <summary>
        /// The total distance since the <see cref="InitialPosition"/> until the current <see cref="CurrentScanFinalPosition"/>.
        /// </summary>
        public float DistanceCovered => Vector3.Distance(InitialPosition, CurrentScanFinalPosition);
        /// <summary>
        /// The current described direction from <see cref="CurrentScanInitialPosition"/> to <see cref="CurrentScanFinalPosition"/>.
        /// </summary>
        public Vector3 CurrentDirection => (CurrentScanFinalPosition - CurrentScanInitialPosition).normalized;
        /// <summary>
        /// The size of the capsule that is projected in the scan.
        /// (The size is proportional to the speed and inversely proportional to the precision {h = Speed/Precision}).
        /// </summary>
        public float CapsuleSize => Speed / Precision;

        /// <summary>
        /// The current initial position used in the Capsule scan.
        /// </summary>
        public Vector3 CurrentScanInitialPosition { get; private set; }
        /// <summary>
        /// The current final position used in the Capsule scan.
        /// </summary>
        public Vector3 CurrentScanFinalPosition { get; private set; }
        /// <summary>
        /// The position from where the scan begins.
        /// </summary>
        public Vector3 InitialPosition { get; private set; }

        public LayerMask ScanableLayers { get; set; }
        public Vector3 InitialDirection { get; set; }
        public Vector3 HitPoint { get; private set; }
        public float MaxDistance { get; set; }
        public float ScanRadius { get; set; }
        public float Speed { get; set; }
        public bool DisableOnHit { get; set; }
        public int Precision { get; set; }

        /// <summary>
        /// The precision which the hit solver algorithm uses to try and find the hit point.
        /// </summary>
        public ushort HitSolverPrecision { get; set; }
        public float HitSolverDepth { get; set; }
        public bool Debug { get; set; }

        bool scanSuccessfull = false;

        public DirectionalScan(DirectionalConfig config)
        {
            this.Precision = Mathf.Max(1, config.Precision);

            this.InitialDirection = config.InitialDirection;
            this.InitialPosition = config.InitialPosition;
            this.ScanableLayers = config.ScanableLayers;

            this.DisableOnHit = config.DisableOnHit;
            this.Debug = config.Debug;

            this.MaxDistance = config.MaxDistance;
            this.ScanRadius = config.ScanRadius;
            this.Speed = config.Speed;

            this.HitSolverPrecision = 2;
            this.HitSolverDepth = .1f;

            // These two pre calculations are needed
            CurrentScanInitialPosition = this.InitialPosition;
            CurrentScanFinalPosition = CurrentScanInitialPosition + InitialDirection * Speed * Time.deltaTime;
        }

        /// <summary>
        /// Returns the <see cref="RaycastHit"/> information of the precise point the capsule scan overlapped.
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public RaycastHit GetHitRegistrationOnCapsuleOverlap(float offset = 1.0f)
        {
            RaycastHit hitResult = new RaycastHit();

            Quaternion rotation = Quaternion.LookRotation((CurrentScanInitialPosition - CurrentScanFinalPosition).normalized, Vector3.up);
            Vector3 vectorRight = rotation * Vector3.right;
            Vector3 vectorUp = rotation * Vector3.up;
            float distance = offset + CurrentDistance;

            // Scans lef/center/right towards the direction
            for (int i = -1; i < 2; i++)
                if (Physics.Raycast(CurrentScanInitialPosition + vectorRight * offset * i, CurrentDirection, out hitResult, distance, ScanableLayers))
                    return hitResult;

            // Scans down/center/up towards the direction
            for (int i = -1; i < 2; i++)
                if (Physics.Raycast(CurrentScanInitialPosition + vectorUp * offset * i, CurrentDirection, out hitResult, distance, ScanableLayers))
                    return hitResult;

            return hitResult;
        }
        /// <summary>
        /// Returns all the colliders currently intercepting the capsule described by:
        /// (<see cref="InitialPosition"/>,
        /// <see cref="CurrentScanFinalPosition"/>,
        /// <see cref="ScanRadius"/>).
        /// </summary>
        /// <returns></returns>
        public Collider[] GetOvelappingColliders()
        {
            return Physics.OverlapCapsule(CurrentScanInitialPosition, CurrentScanFinalPosition, ScanRadius, ScanableLayers);
        }
        /// <summary>
        /// Checks if scan process hasn't reach it's maximum distance yet.
        /// </summary>
        /// <returns></returns>
        public bool ReachedMaxTravelDistance()
        {
            return DistanceCovered > MaxDistance;
        }
        public bool Scan()
        {
            // If DisableOnHit is enabled, only proceeds if
            // we haven't already scanned something
            if (DisableOnHit && !scanSuccessfull &&
                !ReachedMaxTravelDistance())
            {
                for (int i = 0; i < Precision; i++)
                {
                    CurrentScanFinalPosition = CurrentScanInitialPosition + (InitialDirection * (Speed / Precision));

                    scanSuccessfull = Physics.CheckCapsule(CurrentScanInitialPosition, CurrentScanFinalPosition, ScanRadius, ScanableLayers);
                    if (Debug) DebugMethod(scanSuccessfull);

                    // If the capsule cast yelded results
                    if (scanSuccessfull)
                    {
                        // Try raycasnting multiple directions inside the capsule
                        HitPoint = GetHitPoint();

                        // If any hit point was successfully registered
                        if (HitPoint != Vector3.zero)
                        {
                            if (Debug) DebugExtension.DebugPoint(HitPoint, Color.yellow, .8f, 3f);
                            return true;
                        }

                        // If none was successfully registered
                        else scanSuccessfull = false;
                    }

                    CurrentScanInitialPosition = CurrentScanFinalPosition;
                }
            }

            return false;
        }


        void DebugMethod(bool scanSuccessfull)
        {
            DebugExtension.DebugCapsule(CurrentScanInitialPosition, CurrentScanFinalPosition, scanSuccessfull ? Color.green : Color.red, ScanRadius, 3f);
        }

        Vector3 GetHitPoint()
        {
            RaycastHit hit;
            if (Physics.Raycast(new Ray(CurrentScanInitialPosition, InitialDirection), out hit, CapsuleSize + HitSolverDepth, ScanableLayers))
                return hit.point;
            
            else
            {
                Vector3 side;
                for (int i = HitSolverPrecision - 1; i >= 0; i--)
                {
                    Vector3 raycastStart = CurrentScanInitialPosition + CurrentDirection * CapsuleSize * i / HitSolverPrecision;

                    // Raycasts left and right
                    for (int j = 0; j < 2; j++)
                    {
                        if (j == 0) side = Vector3.Cross(CurrentDirection, Vector3.down);
                        else side = Vector3.Cross(CurrentDirection, Vector3.up);

                        Vector3 rayDirection = (CurrentDirection * CapsuleSize / HitSolverPrecision + side * ScanRadius);
                        DebugExtension.DebugArrow(raycastStart, rayDirection, Color.blue, 3f);

                        if (Physics.Raycast(new Ray(raycastStart, rayDirection), out hit, rayDirection.magnitude + HitSolverDepth, ScanableLayers))
                            return hit.point;
                    }

                    // Raycasts up and down
                    for (int k = 0; k < 2; k++)
                    {
                        if (k == 0) side = Vector3.down;
                        else side = Vector3.up;

                        Vector3 rayDirection = (CurrentDirection * CapsuleSize / HitSolverPrecision + side * ScanRadius);
                        DebugExtension.DebugArrow(raycastStart, rayDirection, Color.blue, 3f);

                        if (Physics.Raycast(new Ray(raycastStart, rayDirection), out hit, rayDirection.magnitude + HitSolverDepth, ScanableLayers))
                            return hit.point;
                    }
                }
            }

            return hit.point;
        }
    }
}