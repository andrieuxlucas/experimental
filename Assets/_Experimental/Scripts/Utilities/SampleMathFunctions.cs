﻿namespace Utitlities.Math
{
    public class LinearFunction : MathFunction
    {
        public override float Value { get => value; set => this.value = value; }

        public override void Step(float step)
        {
            value += step;
        }

        private float value;
    }

    public class QuadraticFunction : MathFunction
    {
        public override float Value { get => value * value; set => this.value = value; }

        public override void Step(float step)
        {
            value += step;
        }

        private float value;
    }

    public class ExpFunction : MathFunction
    {
        ExpFunction(float exp = 3) { this.exp = exp; }

        public override float Value { get => (float)System.Math.Pow(value, exp); set => this.value = value; }
        public override void Step(float step) { value += step; }

        float value, exp;
    }
}