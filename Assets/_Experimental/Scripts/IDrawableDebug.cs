﻿/**
 *	IDrawableDebug.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/01/2019	(dd/mm/yy)
 */

public interface IDrawableDebug
{
    void Debug();
}