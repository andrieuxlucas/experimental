﻿/**
 *	DepthOfFieldTask.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	14/08/2018	(dd/mm/yy)
 */

using UnityEngine.Rendering.PostProcessing;
using UnityEngine;

public class DepthOfFieldTask : Task
{
    [SerializeField] float _duration;
    [SerializeField] float _aperture0, _aperture1;
    [SerializeField] float _focalLenght0, _focalLenght1;

    PostProcessProfile _postProcessingProfile;

    public DepthOfFieldTask(MonoBehaviour component) : base(component) { }
    public DepthOfFieldTask(MonoBehaviour component, PostProcessProfile postProcessingProfile) : this(component) {
        this._postProcessingProfile = postProcessingProfile;
    }

    public void Setup(float duration, float aperture0, float aperture1, float focalLenght0, float focalLenght1)
    {
        _duration = duration;
        _aperture0 = aperture0;
        _aperture1 = aperture1;
        _focalLenght0 = focalLenght0;
        _focalLenght1 = focalLenght1;
    }
    protected override void Update(float delta)
    {
        _postProcessingProfile.GetSetting<DepthOfField>().aperture.value = Mathf.Lerp(_aperture0, _aperture1, delta);
        _postProcessingProfile.GetSetting<DepthOfField>().focalLength.value = Mathf.Lerp(_focalLenght0, _focalLenght1, delta);
    }
    public override float GetDuration()
    {
        return _duration;
    }
}