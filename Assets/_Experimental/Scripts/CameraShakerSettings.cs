﻿/**
 *	CameraShakerSettings.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/08/2018	(dd/mm/yy)
 */

using System;

[Serializable]public struct CameraShakerSettings
{
	public float Roughness;
    public float Magnitude;
    public float FadeInTime;
    public float FadeOutTime;
}