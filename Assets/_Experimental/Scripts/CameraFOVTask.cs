﻿/**
 *	CameraFOVTask.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:		(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFOVTask : TaskFor<PlayerPerspective>
{
    public int AlternativeFOV { get; set; }
    public int DefaultFOV { get; set; }
    public float Duration { get; set; }

    public CameraFOVTask(PlayerPerspective component) : base(component) { }

    public override float GetDuration() { return Duration; }

    protected override void OnPreUpdate()
    {
        SpecializedComponent.WorldCamera.fieldOfView = AlternativeFOV;
    }

    protected override void Update(float delta)
    {
        SpecializedComponent.WorldCamera.fieldOfView = Mathf.Lerp(AlternativeFOV, DefaultFOV, delta);
    }

    protected override void OnPostUpdate()
    {
        SpecializedComponent.WorldCamera  .fieldOfView = DefaultFOV;
    }
}