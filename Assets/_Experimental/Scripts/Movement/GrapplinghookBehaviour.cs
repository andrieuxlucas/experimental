﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

using Utitlities.ScanningMethods;
using Utitlities.Math;

using NaughtyAttributes;

public class GrapplinghookBehaviour : MonoBehaviour
{
    public enum TimeoutOption
    {
        CheckPosition, CheckDuration
    }

    public bool IsGrappled => isActive;
    public bool IsActive => isActive;

    [Header("References")]
    [SerializeField] CharacterMovimentComponent moveComponent;

    [Header("On - Activation")]
    [SerializeField] float maxDistance = 30.0f;
    [SerializeField] float flyingSpeed = 3.0f;
    [SerializeField] float collisionRadius = 0.22f;

    [Header("On - Release")]
    [SerializeField] float releaseVelocityMultiplier = 0.75f;
    [SerializeField] float releaseDistance = 2.0f;
    [SerializeField] float releaseTimeout = 1.0f;

    [Header("On - Pulling")]
    public float pull = 10f;
    public float movePull = 3.0f;
    public float slerpVal = 2f;
    public float maxFocesMagnitude = 60;
    public PlayerController controller;

    [Header("Options - Scan details")]
    [SerializeField] LayerMask grappableLayers;
    [SerializeField, Range(1, 10)] int scanPrecision = 3;

    [Header("Options - Visuals")]
    [SerializeField] LineRenderer lineRenderer;
    [SerializeField] Transform grappleThrower;


    [Header("Runtime - Status")]
    [SerializeField, ReadOnly] float ropeLength;
    [SerializeField, ReadOnly] bool isActive = false;
    [SerializeField, ReadOnly] bool isScanning = false;

    Vector3 latchedPoint;
    DirectionalScan dScan;
    Coroutine grappleRoutine;

    public void TryGrapple(Ray grappling)
    {
        if (isActive && grappleRoutine != null) Release();

        dScan = new DirectionalScan(new DirectionalConfig()
        {
            Debug = true,
            DisableOnHit = true,
            InitialDirection = grappling.direction,
            InitialPosition = grappling.origin,
            MaxDistance = maxDistance,
            Precision = scanPrecision,
            ScanableLayers = grappableLayers,
            ScanRadius = collisionRadius,
            Speed = flyingSpeed
        });

        isScanning = true;
        lineRenderer.enabled = true;
    }

    public void Release()
    {
        DisableLineRenderer();

        isActive = false;
        moveComponent.UseGravity = true;
        moveComponent.MovementRestriction = MovementRestriction.None;
        moveComponent.DontLimitExternalForcesMagnitude();

        //Vector3 velocity = moveComponent.MovimentVector;
        //velocity.y = 0.0f;

        //moveComponent.AddForce(moveComponent.MovimentVector.normalized * detachForceMultiplier);
        Vector3 detachForce = new Vector3
        {
            x = moveComponent.MovimentVector.x,
            y = moveComponent.MovimentVector.y < 0 ? 0 : moveComponent.MovimentVector.y,
            z = moveComponent.MovimentVector.z
        };
        moveComponent.AddDiferentialForce(moveComponent.MovimentVector * releaseVelocityMultiplier, 1.0f);

        if (grappleRoutine != null)
            StopCoroutine(grappleRoutine);
    }

    private void BeginGrapple()
    {


        isScanning = false;
        isActive = true;

        grappleRoutine = StartCoroutine(GrappleRoutine());
    }

    private void Update()
    {
        if (isScanning)
        {
            if (dScan.Scan())
                BeginGrapple();

            if (dScan.ReachedMaxTravelDistance())
            {
                isScanning = false;
                DisableLineRenderer();
            }
        }

        if (isActive)
            DebugExtension.DebugWireSphere(latchedPoint, Color.yellow, .8f);

        if (lineRenderer) UpdateLineRanderer();
    }

    void UpdateLineRanderer()
    {
        lineRenderer.SetPosition(0, grappleThrower.position);

        if (isScanning)
            lineRenderer.SetPosition(1, dScan.CurrentScanFinalPosition);
        else if (isActive)
            lineRenderer.SetPosition(1, latchedPoint);
    }
    void DisableLineRenderer()
    {
        for (int i = 0; i < lineRenderer.positionCount; i++)
            lineRenderer.SetPosition(i, Vector3.zero);

        lineRenderer.enabled = false;
    }

    Vector3 GetPullDirection()
    {
        return (latchedPoint - moveComponent.transform.position).normalized;
    }
    IEnumerator GrappleRoutine()
    {
        moveComponent.LimitExternalForcesMagnitude(maxFocesMagnitude);
        moveComponent.MovementRestriction = MovementRestriction.All;
        moveComponent.UseGravity = false;
        latchedPoint = dScan.HitPoint;

        Vector3 pulldir = GetPullDirection();
        while (isActive)
        {
            pulldir = Vector3.Slerp(pulldir, GetPullDirection(), Time.deltaTime * slerpVal);

            Vector3 dir = new Vector3(controller.GetAnalogHorizontalInput(), 0, controller.GetAnalogVerticalInput());
            moveComponent.AddForce(moveComponent.transform.TransformDirection(dir) * movePull * Time.deltaTime);
            moveComponent.AddForce(pulldir * pull * Time.deltaTime);

            yield return null;
        }
    }
}