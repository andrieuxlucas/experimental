﻿/**
 *	CrouchTask.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/08/2018	(dd/mm/yy)
 */

using UnityEngine;
public class CrouchTask : TaskFor<CharacterMovimentComponent>
{
    public CrouchTask(CharacterMovimentComponent behaviour) : base(behaviour) { }

    public override float GetDuration()
    {
        return SpecializedComponent.CrouchDuration;
    }

    protected override void Update(float delta)
    {
        SpecializedComponent.Controller.height =
            Mathf.Lerp
            (
                SpecializedComponent.StandingHeight,
                SpecializedComponent.CrouchedHeight,
                delta
            );
    }

    protected override void OnPostUpdate()
    {
        SpecializedComponent.Controller.height = SpecializedComponent.CrouchedHeight;
    }
}