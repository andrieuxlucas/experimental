﻿/**
 *	CharacterMovimentComponent.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using NaughtyAttributes;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public enum MovementRestriction
{
    None, Forward, Sideways, All
}

[RequireComponent(typeof(CharacterController))]
public class CharacterMovimentComponent : MovementComponent
{
    private const float GROUNDED_GRAVITY_MULTIPLIER = 100.0f;

    public delegate void FallAction(FallInfo fallInformation);

    public event FallAction BeginFall;
    public event FallAction FinishFall;

    public bool UpdatingInputs { get { return _updatingInputs; } set { _updatingInputs = value; } }
    public bool UseFriction { get { return _useFriction; } set { _useFriction = value; } }
    public bool UseGravity { get { return _useGravity; } set { _useGravity = value; } }

    public bool IsPowerSliding { get { return MovementState.Equals(MovementState.Running) && CrouchState.Equals(CrouchState.Crouching); } }
    public bool IsRunning { get { return MovementState.Equals(MovementState.Running); } }
    public bool IsCrouched { get { return CrouchState.Equals(CrouchState.Crouching); } }
    public bool IsGrounded { get { return AerialState.Equals(AerialState.Idle); } }

    public float HardLandingHeightThreshold { get { return _hardLandingHeightThreshold; } }

    public float PowerSlideDuration { get { return _powerSlideDuration; } set { _powerSlideDuration = value; } }
    public float CrouchedHeight { get { return _crouchingHeight; } set { _crouchingHeight = value; } }
    public float StandingHeight { get { return _standingHeight; } set { _standingHeight = value; } }
    public float CrouchDuration { get { return _crouchDuration; } set { _crouchDuration = value; } }

    public float WalkSpeed { get { return _walkingSpeed; } set { _walkingSpeed = value; } }
    public float RunSpeed { get { return _runningSpeed; } set { _runningSpeed = value; } }

    public MovementRestriction MovementRestriction { get => _restriction; set => _restriction = value; }
    public LayerMask ScanableLayers { get => _scanableLayers; set { _scanableLayers = value;} }
    public Vector3 LocalVelocity { get { return transform.InverseTransformDirection(_movementVector); } }
    public Vector3 ForwardVelocity { get { return Vector3.forward * LocalVelocity.z; } }
    public Vector3 ExternalForcesVector { get { return _externalForces; } }
    public Vector3 MovimentVector { get { return _movementVector; } }

    public CharacterController Controller { get { return _characterController; } }

    [Header("Layers")]
    [SerializeField] LayerMask _scanableLayers;

    [Header("Layers")]
    [SerializeField] MovementRestriction _restriction = MovementRestriction.None;

    [Header("References")]
    [SerializeField] private CharacterController _characterController;

    [Header("Values - Vertical movement")]
    [SerializeField, Range(0, 1)] private float _airControl = 1f;
    [SerializeField] private float _gravity = Physics.gravity.y;
    [SerializeField] private float _maxVerticalSpeed = 20.0f;
    [SerializeField] private float _jumpForce = 10.0f;
    [SerializeField] private float _hardLandingHeightThreshold = 20.0f;

    [Header("Values - Horizontal movement")]
    [SerializeField] private float _walkingSpeed = 6.0f;
    [SerializeField] private float _runningSpeed = 10.0f;
    [SerializeField] private float _stopRunningDelay = 0.25f;

    [Header("Values - Crouch")]
    [SerializeField] private float _powerSlideDuration = 0.5f;
    [SerializeField] private float _crouchingHeight = 0.8f;
    [SerializeField] private float _standingHeight = 1.8f;
    [SerializeField] private float _crouchDuration = 0.3f;

    [Header("Options")]
    [SerializeField] private bool _useDelayOnStopRunning = true;
    [SerializeField] private bool _useGravity = true;

    [SerializeField] private bool _updatingInputs = true;
    [SerializeField] private bool _useFriction = true;

    [Header("External forces")]
    [SerializeField] private float _externalForcesFriction = 4.0f;
    [SerializeField] private float _externalForcesThreshold = 0.5f;
    [SerializeField] bool _limitEForcesMag = false;
    [ShowIf("_limitEForcesMag")] [SerializeField] private float _eForcesMaxMag = Mathf.Infinity;


    IMovementRunBehaviour _runBehaviour;
    CollisionFlags _collisionFlags;

    Vector3 _horizontalVector;
    Vector3 _externalForces;
    Vector3 _verticalVector;
    Vector3 _movementVector;

    Coroutine _delayedStopRunningRoutine;
    PowerSlideTask _powerSlideTask;
    CrouchTask _crouchTask;
    StandTask _standTask;
    FallInfo _fallInfo;

    Dictionary<int,Vector3> diferentialForces = new Dictionary<int,Vector3>();
    static int index = 0;

    private void Awake()
    {
        if (!_characterController) _characterController = GetComponent<CharacterController>();
        if (!_characterController) _characterController = GetComponentInChildren<CharacterController>();

        _fallInfo = new FallInfo(_characterController, _hardLandingHeightThreshold);

        _powerSlideTask = new PowerSlideTask(this);
        _crouchTask = new CrouchTask(this);
        _standTask = new StandTask(this);

        _runBehaviour = new ForwardPositiveRunBehaviour();
    }
    private void Start()
    {
        MovementState = MovementState.Walking;
        CrouchState = CrouchState.Standing;
        AerialState = AerialState.None;

        if (_scanableLayers == 0)
        {
            Debug.LogErrorFormat("@{0} - ScanableLayer is not set. Component may not work properly!", GetType().Name);
        }
    }
    private void Update()
    {
        if (_characterController)
        {
            AerialState __cachedState = AerialState;
            AerialState = UpdateAerialState(_verticalVector);

            if (AerialState != __cachedState)
                    HandleAerialStateChange(AerialState);

            if(_useGravity)
                if (CapsuleCastFeetYieldedResults())
                    if (_characterController.isGrounded && AerialState.Equals(AerialState.Idle))
                        _verticalVector = Vector3.down * GROUNDED_GRAVITY_MULTIPLIER;


            AddGravityToVerticalVector();

            _movementVector = transform.TransformDirection(_horizontalVector) + _verticalVector + GetExternalForcesVector();

            _collisionFlags = _characterController.Move(_movementVector * Time.deltaTime);

            ClearDirectionalMoviment();

            if (_useFriction)
                ApplyDragToExternalForcesVector();
        }
    }

    public override void Move(float sidewaysMovement, float forwardMovement)
    {
        if(_updatingInputs)
        {
            float __controlAmount = AerialState.Equals(AerialState.Idle) ? 1.0f : _airControl;
            float __currentSpeed = IsRunning ? _runningSpeed : _walkingSpeed;

            _horizontalVector += CrunchedMovementVector(sidewaysMovement, forwardMovement) * __currentSpeed * __controlAmount;
            _horizontalVector = Vector3.ClampMagnitude(_horizontalVector, __currentSpeed);
        }
    }

    public void KillMomentum(bool includeExternalForces = false)
    {
        _horizontalVector = _verticalVector = _movementVector = Vector3.zero;

        if (includeExternalForces)
            _externalForces = Vector3.zero;
    }
    public Vector3 TransformMovementVectorInExternalVector()
    { 
        return transform.localToWorldMatrix.MultiplyVector(_horizontalVector + _verticalVector);
    }
    public void KillMovement(Vector3 killIntensity)
    {
        killIntensity = new Vector3()
        {
            x = Mathf.Clamp01(killIntensity.x),
            y = Mathf.Clamp01(killIntensity.y),
            z = Mathf.Clamp01(killIntensity.z)
        };

        _movementVector = new Vector3()
        {
            x = _movementVector.x * (1 - killIntensity.x),
            y = _movementVector.y * (1 - killIntensity.y),
            z = _movementVector.z * (1 - killIntensity.z),
        };

        _horizontalVector.Set(_horizontalVector.x * (1 - killIntensity.x), 0, _horizontalVector.z * (1 - killIntensity.z));

        _verticalVector.Set(0, _verticalVector.y * (1 - killIntensity.y), 0);
    }
    public void AddForce(Vector3 force)
    {
        _externalForces += force;
    }
    public void AddDiferentialForce(Vector3 force, float length)
    {
        StartCoroutine(ApplyDiferentialForceRoutine(index++, force, length));
    }

    public void LimitExternalForcesMagnitude(float magnitude)
    {
        _limitEForcesMag = true;
        _eForcesMaxMag = magnitude;
    }
    public void DontLimitExternalForcesMagnitude()
    {
        _limitEForcesMag = false;
        _eForcesMaxMag = Mathf.Infinity;
    }

    public void PowerSlide()
    {
        _powerSlideTask.StartTask();

        AddForce(TransformMovementVectorInExternalVector());

        KillMomentum();
    }
    public void Crouch()
    {
        if(_characterController.isGrounded)
        {
            if (MovementState.Equals(MovementState.Running) && !_powerSlideTask.IsRunning)
                PowerSlide();
            else
            {
                _standTask.StopTask();
                _crouchTask.StartTask();
            }

            CrouchState = CrouchState.Crouching;
        }
    }
    public void Stand()
    {
        _crouchTask.StopTask();
        _standTask.StartTask();

        CrouchState = CrouchState.Standing;
    }
    public void Jump()
    {
        if (_characterController && _characterController.isGrounded)
        {
            _verticalVector = Vector3.up * _jumpForce;

            if (_verticalVector.y > _maxVerticalSpeed)
                _verticalVector.y = _maxVerticalSpeed;

            if (_powerSlideTask.IsRunning)
            {
                _powerSlideTask.StopTask();
                MovementState = MovementState.Walking;
            }

            if(IsCrouched)
            {
                _standTask.StartTask();
                CrouchState = CrouchState.Standing;
            }
        }
    }

    public void Run(float sidewaysMovement, float forwardMovement)
    {
        if (!MovementState.Equals(MovementState.Running) && _characterController.isGrounded)
        {
            if (_runBehaviour.Run(sidewaysMovement, forwardMovement))
            {
                if (_useDelayOnStopRunning)
                    ResetStopRunningDelayed();

                MovementState = MovementState.Running;
            }
        }
    }
    public void StopRun()
    {
        if (MovementState.Equals(MovementState.Running) && _characterController.isGrounded)
        {
            if (_useDelayOnStopRunning)
            {
                if (_delayedStopRunningRoutine == null)
                    StartStopRunningDelayed();
            }

            else MovementState = MovementState.Walking;
        }
    }

    private void HandleAerialStateChange(AerialState newAerialState)
    {
        switch (newAerialState)
        {
            case AerialState.Falling: OnBeginFall(); break;

            case AerialState.Idle: OnFinishFall(); break;

            case AerialState.Jumping: break;
        }
    }
    private AerialState UpdateAerialState(Vector3 verticalVector)
    {
        if (verticalVector.y < 0)           return AerialState.Falling;

        else if (verticalVector.y > 0)      return AerialState.Jumping;

        else                                return AerialState.Idle;
    }
    private Vector3 GetExternalForcesVector()
    {
        Vector3 __return = _externalForces;
        foreach (var vector in diferentialForces.Values)
            __return += vector;

        if (_limitEForcesMag)
            __return = Vector3.ClampMagnitude(__return, _eForcesMaxMag);

        return (__return.magnitude > _externalForcesThreshold ? __return : Vector3.zero);

        if (_limitEForcesMag)
            _externalForces = Vector3.ClampMagnitude(_externalForces, _eForcesMaxMag);

        return (_externalForces.magnitude > _externalForcesThreshold ? _externalForces : Vector3.zero);
    }

    private void ApplyDragToExternalForcesVector()
    {
        _externalForces -= _externalForces * _externalForcesFriction * Time.deltaTime;
    }
    private bool CapsuleCastFeetYieldedResults()
    {
        Vector3 __characterCenter = _characterController.transform.position;
        Vector3 __belowCharacterFeet = _characterController.transform.position + Vector3.down * (_characterController.height / 1.8f);
        float __castRadius = _characterController.radius / 2.0f;

        DebugExtension.DebugCapsule(__characterCenter, __belowCharacterFeet, Color.red, __castRadius);

        return Physics.CheckCapsule(__characterCenter, __belowCharacterFeet, __castRadius, _scanableLayers);
    }
    private void AddGravityToVerticalVector()
    {
        if(_useGravity)
        {
            if (_verticalVector.y > -_maxVerticalSpeed)
                _verticalVector += Vector3.down * _gravity * Time.deltaTime;
            else
                _verticalVector.y = -_maxVerticalSpeed;
        }
    }
    private void ClearDirectionalMoviment()
    {
        if (_characterController.isGrounded)
        {
            _verticalVector = Vector3.zero;

            if (_useFriction)
                _horizontalVector = Vector3.zero;
        }
    }

    private void ResetStopRunningDelayed()
    {
        if (_delayedStopRunningRoutine != null)
        {
            StopCoroutine(_delayedStopRunningRoutine);
            _delayedStopRunningRoutine = null;
        }
    }
    private void StartStopRunningDelayed()
    {
        _delayedStopRunningRoutine = StartCoroutine(DelayedStopRunningRoutine(_stopRunningDelay));
    }

    private void OnFinishFall()
    {
        _fallInfo.FinishFall();

        if (FinishFall != null)
            FinishFall(_fallInfo);
    }
    private void OnBeginFall()
    {
        _fallInfo.BeginFall();

        if (BeginFall != null)
            BeginFall(_fallInfo);
    }

    Vector3 CrunchedMovementVector(float lateralMovement, float frontalMovement)
    {
        return new Vector3(CrunchScalarLateralMovement(lateralMovement), 0, CrunchScalarFrontalMovement(frontalMovement));
    }
    float CrunchScalarLateralMovement(float lateralMovement)
    {
        return lateralMovement * (_restriction == MovementRestriction.Sideways || _restriction == MovementRestriction.All ? 0 : 1);
    }
    float CrunchScalarFrontalMovement(float frontalMovement)
    {
        return frontalMovement * (_restriction == MovementRestriction.Forward || _restriction == MovementRestriction.All ? 0 : 1);
    }

    IEnumerator DelayedStopRunningRoutine(float delay)
    {
        if(delay > 0)   yield return new WaitForSeconds(delay);

        if(!CrouchState.Equals(CrouchState.Crouching))
            MovementState = MovementState.Walking;

        _delayedStopRunningRoutine = null;
    }
    IEnumerator ApplyDiferentialForceRoutine(int index, Vector3 force, float length)
    {
        diferentialForces.Add(index, force);
        for (float elapsed = 0; elapsed < length; elapsed += Time.deltaTime)
        {
            diferentialForces[index] = Vector3.Lerp(force, Vector3.zero, elapsed / length);
            yield return null;
        }
        diferentialForces.Remove(index);
    }
}