﻿/**
 *	MovementComponent.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	24/02/2019 (dd/mm/yy)
 */

using UnityEngine;

public abstract class MovementComponent : MonoBehaviour
{
    public delegate void MovementStateAction(MovementState movementState, CrouchState crouchState, AerialState aerialState);

    public event MovementStateAction MovementStateChanged;

    public AllowedRunMode RunMode
    {
        get => _allowedRunMode;
        set => _allowedRunMode = value;
    }

    public MovementState MovementState
    {
        get => _movementState;
        protected set
        {
            if (!_movementState.Equals(value))
            {
                _movementState = value;

                InvokeMovementChangedDelegate();
            }
        }
    }

    public CrouchState CrouchState
    {
        get => _crouchState;
        protected set
        {
            if(!_crouchState.Equals(value))
            {
                _crouchState = value;

                InvokeMovementChangedDelegate();
            }
        }
    }

    public AerialState AerialState
    {
        get => _aerialState;
        protected set
        {
            if(!_aerialState.Equals(value))
            {
                _aerialState = value;

                InvokeMovementChangedDelegate();
            }
        }
    }

    [Header("States")]
    [SerializeField] private AllowedRunMode _allowedRunMode;
    [SerializeField] private MovementState _movementState;
    [SerializeField] private CrouchState _crouchState;
    [SerializeField] private AerialState _aerialState;

    public abstract void Move(float sidewaysMovement, float forwardMovement);

    void InvokeMovementChangedDelegate()
    {
        MovementStateChanged?.Invoke(_movementState, _crouchState, _aerialState);
    }
}