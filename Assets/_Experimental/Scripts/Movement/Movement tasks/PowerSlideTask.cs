﻿/**
 *	PowerSlideTask.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	21/01/2019	(dd/mm/yy)
 */

public class PowerSlideTask : TaskFor<CharacterMovimentComponent>
{
    public PowerSlideTask(CharacterMovimentComponent component) : base(component) { }

    public override float GetDuration() { return SpecializedComponent.PowerSlideDuration; }


    public override void StopTask()
    {
        base.StopTask();

        OnPostUpdate();
    }


    protected override void Update(float delta) { }

    protected override void OnPostUpdate()
    {
        SpecializedComponent.Stand();

        SpecializedComponent.UseFriction = true;

        SpecializedComponent.UpdatingInputs = true;
    }

    protected override void OnPreUpdate()
    {
        SpecializedComponent.Crouch();

        SpecializedComponent.UseFriction = false;

        SpecializedComponent.UpdatingInputs = false;
    }
}