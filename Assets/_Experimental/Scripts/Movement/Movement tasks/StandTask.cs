﻿/**
 *	StandTask.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/08/2018	(dd/mm/yy)
 */

using UnityEngine;
public class StandTask : TaskFor<CharacterMovimentComponent>
{
    public StandTask(CharacterMovimentComponent component) : base(component) { }

    public override float GetDuration()
    {
        return SpecializedComponent.CrouchDuration;
    }

    protected override void Update(float delta)
    {
        float __lerpValue = Mathf.Lerp
            (
                SpecializedComponent.CrouchedHeight,
                SpecializedComponent.StandingHeight,
                delta
            );

        SpecializedComponent.Controller.height = __lerpValue;
        SpecializedComponent.Controller.transform.Translate(new Vector3(0, (__lerpValue / 2)*Time.deltaTime, 0));
    }

    protected override void OnPostUpdate()
    {
        SpecializedComponent.Controller.height = SpecializedComponent.StandingHeight;
    }
}