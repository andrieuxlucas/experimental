﻿/**
 *	WallrideBehaviour.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	22/02/2019	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class WallrideBehaviour : MonoBehaviour
{
    public Vector3 Feet { get { return transform.position - (transform.up * (_height / 2)); } }
    public Vector3 FeetSides { get { return transform.right * _detectionReach; } }
    public WallrideState State { get { return _wallrideState; } }

    public bool IsWallriding { get { return _wallrideState.Equals(WallrideState.Wallriding); } }
    public bool ValidObjectInRange { get => _validObjectInRange; }

    [Header("Physics")]
    [SerializeField] LayerMask _collisionLayer;

    [Header("References")]
    [SerializeField] CharacterMovimentComponent _movementComponent;

    [Header("Values")]
    [SerializeField] float _height;
    [SerializeField] float _detectionRadius = 0.35f;
    [SerializeField] float _detectionReach = 1.0f;

    [Header("State")]
    [SerializeField] WallrideState _wallrideState;
    [SerializeField] bool _hasWallrideInput;
    [SerializeField] bool _validObjectInRange;

    CapsuleProjection _feetProjection;

    private void OnDrawGizmos()
    {
        _feetProjection.Debug(Color.green);
    }

    private void Awake()
    {
        if (!_movementComponent) _movementComponent = GetComponent<CharacterMovimentComponent>();

        _feetProjection = new CapsuleProjection();
    }

    private void Start()
    {
        _wallrideState = WallrideState.Idle;
    }

    private void Update()
    {
        _feetProjection.Radius = _detectionRadius;
        _feetProjection.Start = Feet + FeetSides;
        _feetProjection.End = Feet + -FeetSides;

        _validObjectInRange = _feetProjection.IsBeingOverlaped(_collisionLayer);

        if(!IsWallriding)
        {
            if (_hasWallrideInput && _validObjectInRange)
                StartWallride();
        }
        else
        {
            if (!_hasWallrideInput || !_validObjectInRange)
                StopWallride();
        }

        _hasWallrideInput = false;
    }

    public void StartWallride()
    {
        _wallrideState = WallrideState.Wallriding;

        _movementComponent.KillMovement(Vector3.up);
        _movementComponent.AddForce(_movementComponent.TransformMovementVectorInExternalVector());

        _movementComponent.KillMomentum();
        _movementComponent.UseGravity = false;
        _movementComponent.UseFriction = false;
    }

    public void StopWallride()
    {
        _wallrideState = WallrideState.Idle;
        _movementComponent.UseGravity = true;
        _movementComponent.UseFriction = true;
    }


    public void HandleWallride(bool wallrideInput, bool jumpInput) { _hasWallrideInput = !jumpInput && wallrideInput; }

    public void UpdateState(MovementState movementState, CrouchState crouchState, AerialState aerialState)
    {
        _hasWallrideInput =  movementState.Equals(MovementState.Running) &&
                        !crouchState.Equals(CrouchState.Crouching) &&
                        aerialState.Equals(AerialState.Falling);
        //!aerialState.Equals(AerialState.Idle);
    }
}
