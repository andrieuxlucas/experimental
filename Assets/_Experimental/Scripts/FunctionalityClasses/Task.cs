﻿/**
 *	Task.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	14/08/2018	(dd/mm/yy)
 */

using System;
using System.Collections;

using UnityEngine;

[Serializable]
public abstract class Task
{
    public static implicit operator Coroutine(Task instance)
    {
        return instance._routine;
    }

    private Action OnTaskStart;
    private Action OnTaskStop;
    private Action OnTaskEnd;
    private bool isRunning;

    public MonoBehaviour AttachedComponent { get { return _attachedComponent; } }
    public Coroutine Routine { get { return _routine; } }
    public bool IsRunning { get { return isRunning; } }

    MonoBehaviour _attachedComponent;
    Coroutine _routine;

    public Task(MonoBehaviour component)
    {
        _attachedComponent = component;
    }

    #region Binding methods

    public void BindOnTaskStart(Action method)
    {
        OnTaskStart = method;
    }
    public void BindOnTaskStop(Action method)
    {
        OnTaskStop = method;
    }
    public void BindOnTaskEnd(Action method)
    {
        OnTaskEnd = method;
    }

    public void UnbindOnTaskStart()
    {
        OnTaskStart = null;
    }
    public void UnbindOnTaskStop()
    {
        OnTaskStop = null;
    }
    public void UnbindOnTaskEnd()
    {
        OnTaskEnd = null;
    }

    #endregion Binding methods

    public virtual void StartTask()
    {
        StopTask();

        _routine = _attachedComponent.StartCoroutine(TaskRoutine(GetDuration()));
    }
    public virtual void StopTask()
    {
        if (_routine != null)
            _attachedComponent.StopCoroutine(_routine);

        isRunning = false;

        InvokeOnTaskStop();
    }

    protected void InvokeOnTaskStart()
    {
        if (OnTaskStart != null)
            OnTaskStart();
    }
    protected void InvokeOnTaskStop()
    {
        if (OnTaskStop != null)
            OnTaskStop();
    }
    protected void InvokeOnTaskEnd()
    {
        if (OnTaskEnd != null)
            OnTaskEnd();
    }

    public abstract float GetDuration();

    protected abstract void Update(float delta);
    protected virtual void OnPostUpdate() { }
    protected virtual void OnPreUpdate() { }

    IEnumerator TaskRoutine(float duration)
    {
        isRunning = true;

        OnPreUpdate();

        InvokeOnTaskStart();

        for (float elapsedTime = 0; elapsedTime < duration; elapsedTime += Time.deltaTime)
        {
            Update(Mathf.Clamp01(elapsedTime / duration));

            yield return null;
        }

        OnPostUpdate();

        InvokeOnTaskEnd();

        isRunning = false;
    }
}