﻿/**
 *	TaskFor.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	16/08/2018	(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TaskFor<T> : Task where T : MonoBehaviour
{
    public T SpecializedComponent { get { return (T)AttachedComponent; } }

    public TaskFor(MonoBehaviour component) : base(component)
    {

    }
    public TaskFor(T component) : this((MonoBehaviour)component)
    {

    }
}