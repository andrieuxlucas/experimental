﻿/**
 *	CapsuleProjection.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	12/08/2018	(dd/mm/yy)
 */

using UnityEngine;

/// <summary>
/// Holds the necessary information to create a capsule projection and check
/// this capsule collision and overlap.
/// </summary>
public struct CapsuleProjection
{
    public Vector3 Center
    {
        get
        {
            Vector3 __direction = End - Start;

            return Start + (__direction / 2);
        }
    }
    public Vector3 Start { get; set; }
    public Vector3 End { get; set; }
    public float Height { get; set; }
    public float Radius { get; set; }

    public CapsuleProjection(Vector3 start, Vector3 direction, float radius)
    {
        Radius = radius;

        Start = start;
        End = Start + direction;

        Height = Vector3.Distance(Start, End);
    }

    public CapsuleProjection(Vector3 start, float height, float radius)
    {
        Start = start;

        Height = height;
        Radius = radius;

        End = Start + Vector3.up * Height;
    }


    public bool IsBeingOverlaped(LayerMask layerMask, float floorOffset)
    {
        return Physics.CheckCapsule(Start + (Vector3.up * floorOffset), End, Radius, layerMask);
    }

    public bool IsBeingOverlaped(LayerMask layerMask)
    {
        return Physics.CheckCapsule(Start, End, Radius, layerMask);
    }


    public void Debug(Color debugColor)
    {
        DebugExtension.DrawCapsule(Start, End, debugColor, Radius);
    }
}