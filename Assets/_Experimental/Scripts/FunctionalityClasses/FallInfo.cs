﻿/**
 *	FallInfo.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public class FallInfo
{
    public float FallDelta { get { return Mathf.Abs(Mathf.Min(FallEndHeight - FallBeginHeight, 0.0f)); } }

    public CharacterController Character { get; private set; }
    public Collider FallTarget { get; set; }
    public bool IsHardLanding { get; set; }

	public float FallBeginHeight { get; private set; }
    public float FallEndHeight { get; private set; }
    public float FallThreshold { get; private set; }

    public FallInfo(CharacterController controller, float fallThreshold)
    {
        Character = controller;
        FallThreshold = fallThreshold;

        FallTarget = null;
        IsHardLanding = false;
        FallBeginHeight = FallEndHeight = 0;
    }

    public void BeginFall()
    {
        IsHardLanding = false;
        FallBeginHeight = Character.transform.position.y;
    }
    public void FinishFall()
    {
        FallEndHeight = Character.transform.position.y;

        IsHardLanding = FallDelta >= FallThreshold;
    }
    public bool AssertHardLanding(float deltaThreshold)
    {
        IsHardLanding = FallDelta >= deltaThreshold;

        return IsHardLanding;
    }
}