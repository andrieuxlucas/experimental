﻿/**
 *	RaycastSetup.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	12/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public struct RaycastSetup
{
    public Vector3 FarthestRayPoint { get { return Ray.GetPoint(MaxDistance); } }
    public Vector3 CameraRayDirection { get { return Ray.direction; } }

    public float MaxDistance { get; set; }
    public Camera Camera { get; set; }

    public Ray Ray { get; set; }
    public LayerMask LayerMaks { get; set; }

    public RaycastSetup(Camera camera, LayerMask layerMask, float maxDistance)
    {
        this.Ray = new Ray();
        this.Camera = camera;
        this.LayerMaks = layerMask;
        this.MaxDistance = maxDistance;
    }

    public bool CastRay(out RaycastHit raycastHit)
    {
        return Physics.Raycast(Ray, out raycastHit, MaxDistance, LayerMaks);
    }
}