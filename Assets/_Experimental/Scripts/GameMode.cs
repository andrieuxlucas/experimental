﻿/**
 *	GameMode.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMode : MonoBehaviour
{
    public float fadeInTime;
    public float roughness;
    public float magnitude;

    private void Start()
    {
        OnStart();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.F1))
        {
            EZCameraShake.CameraShaker.Instance.Shake(EZCameraShake.CameraShakePresets.Bump);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            EZCameraShake.CameraShaker.Instance.Shake(EZCameraShake.CameraShakePresets.Explosion);
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            EZCameraShake.CameraShaker.Instance.Shake(EZCameraShake.CameraShakePresets.Earthquake);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            EZCameraShake.CameraShaker.Instance.ShakeOnce(magnitude, roughness, fadeInTime, 1f);
            //ProgrammingResources.Effect.Camera.CameraShake.Instance.TriggerEffect();
        }
    }
    private void OnStart()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
}