﻿/**
 *	DebuggableComponent.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/01/2019	(dd/mm/yy)
 */

using UnityEngine;

public abstract class DebuggableComponent : MonoBehaviour, IDrawableDebug
{
    [Header("Debbugable options")]
    [SerializeField] private bool _debug;

    public void OnDrawGizmos() { if(_debug) Debug(); }

    public abstract void Debug();
}