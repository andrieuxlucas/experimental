﻿/**
 *	TestCanvas.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/08/2018	(dd/mm/yy)
 */

using TMPro;
using UnityEngine;

using System.Collections;
using System;

public class TestCanvas : MonoBehaviour
{
    public float interval = 0.0f;
    public TextMeshProUGUI movimentSpeed;
    public TextMeshProUGUI fallHeight;
    public TextMeshProUGUI movimentVector;
    public TextMeshProUGUI movimentState;
    public TextMeshProUGUI aerialState;
    public TextMeshProUGUI hardLanding;
    public TextMeshProUGUI externalForces;
    public TextMeshProUGUI wallrideState;

    CharacterMovimentComponent moveComp;
    WallrideBehaviour wallride;

    private void OnEnable()
    {
        moveComp = FindObjectOfType<CharacterMovimentComponent>();
        wallride = FindObjectOfType<WallrideBehaviour>();

        moveComp.FinishFall += UpdateFallHeight;
        moveComp.FinishFall += UpdateHardLanding;
    }

    private void UpdateHardLanding(FallInfo fallInformation)
    {
        hardLanding.text = "Hard landing: " + (fallInformation.IsHardLanding ? "Yes" : "No");
        hardLanding.color = fallInformation.IsHardLanding ? Color.red : Color.green;
    }

    private void UpdateFallHeight(FallInfo fallInformation)
    {
        fallHeight.text = "Falling height: " + fallInformation.FallDelta.ToString("0.00");    
    }

    private void Start()
    {
        StartCoroutine(UpdateRoutine(interval));
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }

    IEnumerator UpdateRoutine(float interval)
    {
        while (true)
        {
            movimentSpeed.text = "Moviment speed: " + (moveComp.IsRunning ? moveComp.RunSpeed : moveComp.WalkSpeed);

            externalForces.text = "External forces: " + moveComp.ExternalForcesVector;

            movimentVector.text = "Moviment vector: " + moveComp.MovimentVector;

            movimentState.text = "Moviment state: " + moveComp.MovementState;

            aerialState.text = "Aerial state: " + moveComp.AerialState;

            wallrideState.text = "Walriding: " + (wallride.IsWallriding ? "Yes" : "No");
            wallrideState.color = (wallride.IsWallriding ? Color.green : Color.red);

            yield return interval <= 0 ? null : new WaitForSeconds(interval);
        }
    }
}