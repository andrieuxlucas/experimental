﻿/**
 *	CharaterPerspective.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/06/2019 (dd/mm/yy)
 */

 using UnityEngine;

public abstract class CharacterPerspective : MonoBehaviour
{
    public virtual Vector3 CameraForward => GetCamera() ? GetCamera().transform.forward : Vector3.zero;
    public virtual Vector3 CameraPosition => GetCamera() ? GetCamera().transform.position : Vector3.zero;
    public virtual Ray RayFromCameraToMouse => GetCamera() ? GetCamera().ScreenPointToRay(Input.mousePosition) : default;

    public abstract Camera GetCamera();
    public abstract Transform GetPlayerBody();
    public abstract LayerMask GetScanableLayers();
    public abstract void SetScanableLayers(LayerMask scanableLayers);
    public abstract void TurnView(float horizontalTurn, float verticalTurn);
}