﻿/**
 *	CharacterStatus.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	05/03/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;

using UnityEngine;

public class CharacterStatus : MonoBehaviour, IEffectable
{
    const float updateRate = 0.25f;

    [Header("References")]
    [SerializeField] Charater owner;

    [Header("Ongoing effects")]
    [SerializeField] List<Effect> effectsList;

    void Awake()
    {
        effectsList = new List<Effect>();
    }

    void Start()
    {
        StartCoroutine(ApplyEffectsCoroutine());
    }


    public void ApplyEffect(Effect effect)
    {
        if (ContainsEffectOfType(effect, out Effect e))
            e.ResetDuration();

        else effectsList.Add(effect);
    }

    public void RemoveEffect(Effect effect)
    {
        if(ContainsEffectOfType(effect, out Effect e))
        {
            e.Remove(owner, updateRate, effectsList.ToArray());
            effectsList.Remove(e);
        }
    }


    public bool HasEffect(Effect effect)
    {
        foreach (Effect e in effectsList)
            if (e.GetType().Equals(effect.GetType()))
                return true;

        return false;
    }

    public bool HasEffect<T>() where T : Effect
    {
        foreach (Effect e in effectsList)
            if (e.GetType().Equals(typeof(T)))
                return true;

        return false;
    }


    bool ContainsEffectOfType(Effect effect, out Effect effectReference)
    {
        foreach (Effect e in effectsList)
        {
            if (e.GetType().Equals(effect.GetType()))
            {
                effectReference = e;
                return true;
            }
        }

        effectReference = null;
        return false;   
    }

    IEnumerator ApplyEffectsCoroutine()
    {
        while(enabled)
        {
            yield return new WaitForSeconds(updateRate);

            for (int i = effectsList.Count -1; i >= 0; i--)
            {
                if (effectsList[i].Apply(owner, updateRate, effectsList.ToArray()))
                {
                    effectsList[i].Remove(owner, updateRate, effectsList.ToArray());
                    effectsList.Remove(effectsList[i]);
                }
            }
        }
    }
}