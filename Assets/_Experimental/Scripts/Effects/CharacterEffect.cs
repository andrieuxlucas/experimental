﻿/**
 *	CharacterEffect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	02/03/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;

using UnityEngine;

[CreateAssetMenu(menuName = EFFECT_FOLDER + nameof(CharacterEffect))]
public abstract class CharacterEffect : EffectFor<Charater>
{

}