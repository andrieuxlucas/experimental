﻿/**
 *	Effect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	02/03/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using UnityEngine;

public abstract class Effect : ScriptableObject
{
    protected const string EFFECT_FOLDER = "Effects/";

    public bool IsLifeCycleComplete { get { return isFinite ? false : activeElapsedTime >= duration; } }
    public bool IsFinite { get { return isFinite; } }

    public float Duration { get { return duration; } }
    public string Name { get { return name; } }

    [Header("Properties")]
    [SerializeField] protected string description;
    [SerializeField] protected float duration = 1.0f;
    [SerializeField] protected bool isFinite = true;

    [Header("Runtime")]
    [SerializeField] float activeElapsedTime = 0.0f;

    public bool Apply(object applyTo, float delta, params object[] args)
    {
        activeElapsedTime += delta;

        Run(applyTo, delta, args);

        return IsLifeCycleComplete;
    }

    public void ResetDuration()
    {
        activeElapsedTime = 0.0f;
    }


    public abstract void Remove(object removeFrom, float delta, params object[] args);

    protected abstract void Run(object applyTo, float delta, params object[] args);

    protected T[] GetValuableArguments<T>(object[] args)
    {
        List<T> listOfValuableObjects = new List<T>();

        foreach (var obj in args)
            if (obj is T)
                listOfValuableObjects.Add((T)obj);

        return listOfValuableObjects.ToArray();
    }

    protected T GetValuableArgument<T>(object[] args)
    {
        foreach (var obj in args)
            if (obj is T)
                return (T)obj;

        return default;
    }
}