﻿/**
 *	EffectFor.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	02/03/2019 (dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;

using UnityEngine;

public abstract class EffectFor<T> : Effect where T : Behaviour
{
    string InconsistentTypeMessage(object target)
    {
        return string.Format("@{0} - Target of type: {1} does not match valid target type: {2}!",
            GetType().Name, target.GetType().Name, nameof(T));
    }

    public override void Remove(object removeFrom, float delta, params object[] args)
    {
        if (!(removeFrom is T))
        {
            Debug.LogError(InconsistentTypeMessage(removeFrom));
            return;
        }

        Remove(removeFrom as T, delta, args);
    }

    protected override void Run(object applyTo, float delta, params object[] args)
    {
        if (!(applyTo is T))
        {
            Debug.LogError(InconsistentTypeMessage(applyTo));
            return;
        }

        Run(applyTo as T, delta, args);
    }


    protected abstract void Remove(T removeFrom, float delta, params object[] args);

    protected abstract void Run(T applyTo, float delta, params object[] args);
}