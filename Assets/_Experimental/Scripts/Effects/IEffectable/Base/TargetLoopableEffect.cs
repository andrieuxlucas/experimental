﻿/**
 *	TargetLoopableEffect.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on: 18/05/2018	(dd/mm/yy)
 */

using UnityEngine;

namespace ProgrammingResources.Effect
{
    /// <summary>
    /// Base class for effects with a loop logic that targets a specific component.
    /// </summary>
    /// <typeparam name="ComponentType"></typeparam>
    public abstract class TargetLoopableEffect<ComponentType> : LoopableEffect where ComponentType : Component
    {
        protected ComponentType component;

        /// <summary>
        /// The target component for this effect.
        /// </summary>
        public ComponentType Component { get { return component; } }

        protected override void Awake()
        {
            base.Awake();

            if (!component) component = GetComponent<ComponentType>();
        }
    }
}