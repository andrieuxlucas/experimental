﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProgrammingResources.Effect.Camera
{
    public class CameraShake : TargetLoopableEffect<UnityEngine.Camera>
    {
        private static CameraShake _instance;
        public static CameraShake Instance
        {
            get
            {
                if (!_instance)
                {
                    var __camera = UnityEngine.Camera.main;
                    _instance = __camera.gameObject.AddComponent<CameraShake>();
                }

                return _instance;
            }

            private set
            {
                if (!_instance)
                {
                    _instance = value;
                }
                else if (_instance != value && value != null)
                {
                    Destroy(value.gameObject);
                }
                else if (value == null)
                {
                    _instance = null;
                }
            }
        }

        private enum ShakeType { X_Only, Y_Only, Z_Only, XY, XZ, YZ, XYZ };

        [Header("Shake parameters")]
        [SerializeField] private ShakeType _shakeAxis;
        [SerializeField] private float _shakeMagnitude;

        [Tooltip("As many cameras use a pivot point to be able to apply effects in the camera and also" +
            " apply other unrelated movements, this option allows the camera shake component to apply" +
            " the effect to the pivot and not in the camera itself, this way, not overwriting" +
            " the camera object movement.")]
        [Header("Shake options")]
        [SerializeField] private bool _shakeParent;

        private Vector3 _initialPosition;

        public override void StopEffect()
        {
            base.StopEffect();

            if(_shakeParent) component.transform.parent.localPosition = _initialPosition;
            else component.transform.localPosition = _initialPosition;
        }
        public override void TriggerEffect()
        {
            if (_shakeParent) _initialPosition = component.transform.parent.localPosition;
            else _initialPosition = component.transform.localPosition;

            base.TriggerEffect();
        }
        public override void Effect(float delta)
        {
            Vector3 __shake = CalculateShake();

            if(_shakeParent) component.transform.parent.localPosition = __shake;
            else component.transform.localPosition = __shake;
        }

        protected override void Awake()
        {
            base.Awake();

            Instance = this;

            _initialPosition = component.transform.localPosition;
        }

        /// <summary>
        /// Main method used to calculate the delta movement of the camera.
        /// </summary>
        /// <returns>The shake vector of the next position for the camera.</returns>
        private Vector3 CalculateShake()
        {
            Vector3 __multiplier = Vector3.zero;
            Vector3 __shake = new Vector3
            {
                x = Random.Range(-_shakeMagnitude, _shakeMagnitude),
                y = Random.Range(-_shakeMagnitude, _shakeMagnitude),
                z = Random.Range(-_shakeMagnitude, _shakeMagnitude)
            };

            switch (_shakeAxis)
            {
                case ShakeType.X_Only: __multiplier = Vector3.right; break;
                case ShakeType.Y_Only: __multiplier = Vector3.up; break;
                case ShakeType.Z_Only: __multiplier = Vector3.forward; break;

                case ShakeType.XY: __multiplier = Vector3.right + Vector3.up; break;
                case ShakeType.XZ: __multiplier = Vector3.right + Vector3.forward; break;
                case ShakeType.YZ: __multiplier = Vector3.up + Vector3.forward; break;

                case ShakeType.XYZ: __multiplier = Vector3.one; break;
            }


            __shake = new Vector3
            {
                x = __shake.x * __multiplier.x,
                y = __shake.y * __multiplier.y,
                z = __shake.z * __multiplier.z
            };

            return __shake;
        }

        /// <summary>
        /// Use it to trigger the effect using the inspector. Usefull for quick testing.
        /// </summary>
        [ContextMenu("Trigger effect")]
        private void InspectorTrigger()
        {
            TriggerEffect();
        }
    }
}