﻿/**
 *	IEffectable.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	05/03/2019 (dd/mm/yy)
 */

public interface IEffectable
{
    void ApplyEffect(Effect effect);
    void RemoveEffect(Effect effect);
}