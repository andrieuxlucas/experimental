﻿/**
 *	PermaHeadLock.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	05/03/2019 (dd/mm/yy)
 */

using UnityEngine;

[CreateAssetMenu(menuName = EFFECT_FOLDER + nameof(PermaHeadLock))]
public class PermaHeadLock : CharacterEffect
{
    protected override void Remove(Charater removeFrom, float delta, params object[] args)
    {
        removeFrom.CanTurn = true;
    }

    protected override void Run(Charater applyTo, float delta, params object[] args)
    {
        applyTo.CanTurn = false;
    }
}