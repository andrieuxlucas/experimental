﻿/**
 *	PermaUnmoveable.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	05/03/2019 (dd/mm/yy)
 */

using UnityEngine;

[CreateAssetMenu(menuName = EFFECT_FOLDER + nameof(PermaUnmoveable))]
public class PermaUnmoveable : CharacterEffect
{
    protected override void Remove(Charater removeFrom, float delta, params object[] args)
    {
        removeFrom.CanMove = true;
        removeFrom.CanRun = true;
    }

    protected override void Run(Charater applyTo, float delta, params object[] args)
    {
        applyTo.CanMove = false;
        applyTo.CanRun = false;
    }
}