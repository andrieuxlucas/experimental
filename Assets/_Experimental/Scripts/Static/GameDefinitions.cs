﻿/**
 *	GameDefinitions.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/06/2019 (dd/mm/yy)
 */

using UnityEngine;

public static class GameDefinitions
{
    public static readonly LayerMask AllButPlayerMask = new LayerMask()
    {
        value = LayerMask.GetMask("Default", "MapStatic", "YuME_TileMap")
    };
}