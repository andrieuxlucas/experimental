﻿/**
 *	Charater.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public abstract class Charater : MonoBehaviour
{
    public abstract bool CanMove { get; set; }
    public abstract bool CanRun { get; set; }
    public abstract bool CanTurn { get; set; }
    public abstract bool CanCrouch { get; set; }

    public CharacterStatus StatusComponent { get { return _statusComponent; } }
    public CharacterPerspective Perspective { get { return _playerPerspective; } }
    public CharacterMovimentComponent MovementComponent { get { return _movementComponent; } }

    [Header("Components")]
    [SerializeField] protected CharacterMovimentComponent _movementComponent;
    [SerializeField] protected CharacterPerspective _playerPerspective;
    [SerializeField] protected CharacterStatus _statusComponent;

    [Header("States")]
    [SerializeField] protected bool _canRun = true;
    [SerializeField] protected bool _canMove = true;
    [SerializeField] protected bool _canTurn = true;
    [SerializeField] protected bool _canCrouch = true;

    private void Awake()
    {
        if (!_statusComponent) _statusComponent = GetComponent<CharacterStatus>();
        if (!_playerPerspective) _playerPerspective = GetComponent<CharacterPerspective>();
        if (!_movementComponent) _movementComponent = GetComponent<CharacterMovimentComponent>();
    }


    public virtual void TurnPerspective(float horizontalTurn, float verticalTurn)
    {
        if (_playerPerspective)
            _playerPerspective.TurnView(horizontalTurn, verticalTurn);
    }

    public virtual void Move(float sidewaysMoviment, float forwardMoviment)
    {
        if (_movementComponent)
            _movementComponent.Move(sidewaysMoviment, forwardMoviment);
    }


    public virtual void StopRun()
    {
        _movementComponent.StopRun();
    }
           
    public virtual void Punch()
    {

    }
           
    public virtual void Jump()
    {
        _movementComponent.Jump();
    }
           
    public virtual void Run(float sidewaysMoviment, float forwardMoviment)
    {
        _movementComponent.Run(sidewaysMoviment, forwardMoviment);
    }
}