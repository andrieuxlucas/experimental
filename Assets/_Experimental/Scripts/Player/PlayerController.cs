﻿/**
 *	PlayerController.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using System.Collections;
using UnityEngine;

public class PlayerController : Controller<PlayerCharacter>
{
    #region Overriden functionality 

    public override float GetCamHorizontalInput()
    {
        var __mouseInput = Input.GetAxis("Mouse X");

        return __mouseInput != 0 ? __mouseInput : Input.GetAxis("RHorizontal");
    }

    public override float GetCamVerticalInput()
    {
        var __mouseInput = Input.GetAxis("Mouse Y");

        return __mouseInput != 0 ? __mouseInput : Input.GetAxis("RVertical");
    }


    public override bool GetActionInput() { throw new System.NotImplementedException(); }

    public override float GetHorizontalInput() { return Input.GetAxis("Horizontal"); }

    public override float GetVerticalInput() { return Input.GetAxis("Vertical"); }

    public override bool GetJumpInput() { return Input.GetButton("Jump"); }


    public override bool GetRunInput() { return Input.GetButton("Run") ? Input.GetButton("Run") : Input.GetAxis("Run") != 0; }

    public override bool GetPunchInput() { return Input.GetButtonDown("Punch") && !Input.GetButton("Blink"); }

    #endregion Overriden functionality

    public virtual bool GetCrouchInput() { return Input.GetButtonDown("Crouch"); }

    public virtual bool GetWallrideInput() { return Input.GetButton("Run"); }


    public virtual bool GetBlinkInterruptInput() { return Input.GetButton("Blink") && Input.GetButtonDown("Punch"); }

    /// <summary>
    /// Retrieves the state of the key for finishing the blink.
    /// </summary>
    /// <returns>Is the key pressed?</returns>
    public virtual bool GetBlinkReleaseInput() { return Input.GetButtonUp("Blink"); }

    /// <summary>
    /// Retrieves the state of the key for triggering the blink.
    /// </summary>
    /// <returns>Is the key pressed?</returns>
    public virtual bool GetBlinkPressedInput() { return Input.GetButtonDown("Blink"); }

    public virtual bool GetGrapplinghookPressedInput() { return Input.GetButtonDown("GrapplingHook"); }


    public virtual float GetAnalogHorizontalInput() { return Input.GetAxis("Horizontal_Analog"); }

    public virtual float GetAnalogVerticalInput() { return Input.GetAxis("Vertical_Analog"); }


    /// <summary>
    /// Called each frame by its parent class.
    /// </summary>
    protected override void OnUpdate()
    {
        base.OnUpdate();

        if (_charater.CanMove)
        {
            PlayerPerspective __perspective = (PlayerPerspective)_charater.Perspective;
            if(__perspective)
            {
                __perspective.SetCameraTilt(-GetAnalogHorizontalInput());
            }
        }

        if (GetCrouchInput())
            if(_charater.CanCrouch)
                _charater.Crouch();

        // If we're pressed the blink button
        if (GetBlinkPressedInput())
            _charater.PrepareBlink();

        // If we released the blink button
        else if (GetBlinkReleaseInput())
            _charater.ExecuteBlink();

        // If we pressed the interaction interrupt button
        else if (GetBlinkInterruptInput())
            _charater.InterruptBlink();

        else if (GetGrapplinghookPressedInput())
            _charater.UseGrapplingHook();

        _charater.HandleWallride(GetWallrideInput(), GetJumpInput());
    }
}