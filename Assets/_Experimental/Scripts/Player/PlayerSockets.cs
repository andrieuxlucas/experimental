﻿/**
 *	PlayerSockets.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public class PlayerSockets : MonoBehaviour
{
    [Header("Animation sockets")]
    [SerializeField] private Transform rightHandSocket;
    [SerializeField] private Transform leftHandSocket;
}