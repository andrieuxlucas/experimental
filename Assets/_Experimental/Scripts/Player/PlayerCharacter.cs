﻿/**
 *	PlayerCharacter.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018(dd/mm/yy)
 */

using UnityEngine;

public class PlayerCharacter : Charater
{
    public override bool CanCrouch {
        get {
            return true;
            //return _canCrouch && 
            //    !(
            //        _movimentComponent.CrouchState.Equals(CrouchState.Crouching) &&
            //        _movimentComponent.MovementState.Equals(MovementState.Running)
            //    );
        }
        set { _canCrouch = value; }
    }
    public override bool CanRun {
        get { return (!_movementComponent.IsCrouched && !_blinkComponent.State.Equals(BlinkState.Blinking)) && _canRun; }
        set { _canRun = value; }
    }
    public override bool CanMove {
        get { return !_blinkComponent.State.Equals(BlinkState.Blinking) && _canMove; }
        set { _canMove = value; }
    }
    public override bool CanTurn {
        get { return _canTurn; }
        set { _canTurn = value; }
    }

    [Header("References")]
    [SerializeField] protected GrapplinghookBehaviour _grapplingHook;
    [SerializeField] protected WallrideBehaviour _wallrideBehaviour;
    [SerializeField] protected BlinkComponent _blinkComponent;
    [SerializeField] protected PlayerCameraVFX _cameraVFX;
    [SerializeField] protected PlayerVFX _playerVFX;

    private void Awake()
    {
        if (!_wallrideBehaviour) _wallrideBehaviour = GetComponent<WallrideBehaviour>();
        if (!_blinkComponent) _blinkComponent = GetComponent<BlinkComponent>();
        if (!_cameraVFX) _cameraVFX = GetComponentInChildren<PlayerCameraVFX>();
        if (!_playerVFX) _playerVFX = GetComponent<PlayerVFX>();
    }
    private void OnEnable()
    {
        if (_blinkComponent)
        {
            _blinkComponent.OnBlinkBegin += OnBlinkBegin;
            _blinkComponent.OnBlinkFinish += OnBlinkFinish;
        }

        if (_movementComponent)
        {
            _movementComponent.FinishFall += OnFallFinished;
        }

        _movementComponent.ScanableLayers = GameDefinitions.AllButPlayerMask;
    }
    private void OnDisable()
    {
        if (_blinkComponent)
        {
            _blinkComponent.OnBlinkBegin -= OnBlinkBegin;
            _blinkComponent.OnBlinkFinish -= OnBlinkFinish;
        }

        if (_movementComponent)
        {
            _movementComponent.FinishFall -= OnFallFinished;
        }
    }

    public override void Jump()
    {
        if (_grapplingHook.IsGrappled)
            _grapplingHook.Release();

        else  base.Jump();
    }

    public void PowerSlide()
    {
        if (_movementComponent)
            _movementComponent.PowerSlide();
    }
    public void Crouch()
    {
        if (_movementComponent)
        {
            if (_movementComponent.IsCrouched) _movementComponent.Stand();

            else _movementComponent.Crouch();
        }
    }

    public void PrepareBlink()
    {
        if (_blinkComponent)
            _blinkComponent.PrepareBlink();
    }
    public void ExecuteBlink()
    {
        if (_blinkComponent)
            _blinkComponent.ExecuteBlink();

        if (_grapplingHook)
            _grapplingHook.Release();
    }
    public void InterruptBlink()
    {
        if (_blinkComponent)
            _blinkComponent.InterruptBlink();
    }
    public void UseGrapplingHook()
    {
        if (_grapplingHook)
            _grapplingHook.TryGrapple(new Ray(_playerPerspective.CameraPosition, _playerPerspective.CameraForward));
    }
    public void ReleaseGrapplingHook()
    {
        if (_grapplingHook)
            _grapplingHook.Release();
    }
    public void HandleWallride(bool wallrideInput, bool jumpInput)
    {
        if(_movementComponent.IsRunning && (!_movementComponent.IsGrounded || _wallrideBehaviour.IsWallriding))
        {
            //print(string.Format("Conditions met: R{0} | J{1}", _movimentComponent.IsRunning, !_movimentComponent.IsGrounded));
            _wallrideBehaviour.HandleWallride(wallrideInput, jumpInput);
        }
    }

    private void OnBlinkBegin()
    {
        _playerVFX.PlayBlinkParticles();

        _cameraVFX.ShakeCamera(6f, 4f, 0.1f, 0.8f);
        _cameraVFX.EnableDepthOFField();

        _movementComponent.KillMomentum(true);
        _movementComponent.UseGravity = false;
    }
    private void OnBlinkFinish()
    {
        _playerVFX.StopBlinkParticles();
        _movementComponent.UseGravity = true;
    }
    private void OnFallFinished(FallInfo fallInformation)
    {
        if (fallInformation.IsHardLanding)
            _cameraVFX.ShakeCamera();
    }
}