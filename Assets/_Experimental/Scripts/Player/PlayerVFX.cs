﻿/**
 *	BlinkVFX.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/08/2018	(dd/mm/yy)
 */

using UnityEngine;
using EZCameraShake;
using System.Collections;

public class PlayerVFX : MonoBehaviour
{
    [Header("Particle")]
    [SerializeField] private ParticleSystem _dashParticles;
    [SerializeField] private ParticleSystem _dashParticles2;
    [SerializeField] private ParticleSystem _speedLinesParticles;

    [Header("Values")]
    [SerializeField] private float _fadeInDelay = 0.0f;
    [SerializeField] private float _fadeOutDelay = 0.15f;

    Coroutine _effectRoutine;

    private void Start()
    {
        _dashParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        _dashParticles2.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        _speedLinesParticles.Stop();
    }

    public void PlayBlinkParticles()
    {
        _effectRoutine = StartCoroutine(PlayParticlesRoutine(_fadeInDelay));
    }

    public void StopBlinkParticles()
    {
        _effectRoutine = StartCoroutine(StopParticlesRoutine(_fadeOutDelay));
    }

    IEnumerator PlayParticlesRoutine(float delay)
    {
        if (delay > 0) yield return new WaitForSeconds(delay);

        _dashParticles.Play(true);
        _dashParticles2.Play(true);
        _speedLinesParticles.Play();
    }

    IEnumerator StopParticlesRoutine(float delay)
    {
        if(delay > 0) yield return new WaitForSeconds(delay);

        _dashParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        _dashParticles2.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        _speedLinesParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
    }
}