﻿/**
 *	PlayerPrespective.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using System.Collections;
using UnityEngine;
public class PlayerPerspective : CharacterPerspective
{
    public Camera PlayerCamera { get { return _playerCamera; } }
    public Camera WorldCamera { get { return _worldCamera; } }

    public Vector3 MousePosition => GetCamera() ? GetCamera().ViewportToWorldPoint(Input.mousePosition) : default;
    public Vector3 DirectionToMouse => (MousePosition - GetCamera().transform.position).normalized;

    [Header("Layers")]
    [SerializeField] private LayerMask _scanableLayers;

    [Header("Components")]
    [SerializeField] private Transform _playerBody;
    [SerializeField] private Camera _playerCamera;
    [SerializeField] private Camera _worldCamera;

    [Header("Settings")]
    [SerializeField, Range(0, 90)] float _maxUpAngle = 80.0f;
    [SerializeField, Range(0, 90)] float _maxDownAngle = 80.0f;
    [SerializeField] private float _horizontalTurnRate = 120.0f;
    [SerializeField] private float _verticalTurnRate = 60.0f;

    [Header("Horizontal inclination")]
    [SerializeField, Range(-1, 1)] private float _currentFrontalTilt;
    [SerializeField] private float _maxInclinationAngle = 30.0f;
    [SerializeField] private float _tiltIncreaseRate = .1f;
    [SerializeField] private float _tiltDecreaseRate = .25f;

    private void Awake()
    {
        if (!_playerBody) _playerBody = GetComponent<Transform>();
        if (!_playerCamera) _playerCamera = GetComponent<Camera>();  
    }

    public void SetCameraTilt(float ratio)
    {
        if(ratio != 0)
            _currentFrontalTilt = Mathf.LerpUnclamped(_currentFrontalTilt, ratio, _tiltIncreaseRate);

        else
            _currentFrontalTilt = Mathf.LerpUnclamped(_currentFrontalTilt, 0, _tiltDecreaseRate);

        Vector3 __cachedRotation = _playerCamera.transform.rotation.eulerAngles;
        __cachedRotation.z = 0.0f;

        _playerCamera.transform.rotation = Quaternion.Euler(__cachedRotation) * Quaternion.Euler(Vector3.forward * _maxInclinationAngle * _currentFrontalTilt);
    }

    public override Camera GetCamera()
    {
        return _playerCamera;
    }
    public override Transform GetPlayerBody()
    {
        return _playerBody;
    }
    public override LayerMask GetScanableLayers()
    {
        return _scanableLayers;
    }

    public override void SetScanableLayers(LayerMask scanableLayers)
    {
        this._scanableLayers = scanableLayers;
    }
    public override void TurnView(float horizontalTurn, float verticalTurn)
    {
        if (_playerBody)
            _playerBody.Rotate(Vector3.up * horizontalTurn * _horizontalTurnRate * Time.deltaTime);

        if (_playerCamera)
        {
            //_playerCamera.transform.RotateAround(_playerBody.transform.position, Vector3.up, horizontalTurn * _horizontalTurnRate * Time.deltaTime);
            _playerCamera.transform.Rotate(Vector3.right * verticalTurn * _verticalTurnRate * Time.deltaTime);
        }

        float __dotForwarAndUp = Vector3.Dot(CameraForward, Vector3.up);
        float __angle = Mathf.Acos(__dotForwarAndUp) * Mathf.Rad2Deg;

        // Looking up
        if (verticalTurn < 0)
        {
            // Went through the upper threshold
            if (__angle < (90 - _maxUpAngle))
                _playerCamera.transform.localRotation = Quaternion.Euler(new Vector3(-_maxUpAngle, 0, 0));
        }

        // Looking down
        else if (verticalTurn > 0)
        {
            // Went through the botom threshold
            if (__angle > (90 + _maxDownAngle))
                _playerCamera.transform.localRotation = Quaternion.Euler(new Vector3(_maxDownAngle, 0, 0));
        }
    }

    IEnumerator FovCoroutine(ushort targetFOV, float inSeconds)
    {
        float currentFOV = _worldCamera.fieldOfView;
        for (float step = 0; step < inSeconds; step += Time.deltaTime)
        {
            _worldCamera.fieldOfView = Mathf.Lerp(currentFOV, targetFOV, step / inSeconds);
            yield return null;
        }
    }
}