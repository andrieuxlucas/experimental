﻿/**
 *	PlayerCameraVFX.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	13/06/2019 (dd/mm/yy)
 */

using UnityEngine.Rendering.PostProcessing;
using UnityEngine;

using EZCameraShake;

public class PlayerCameraVFX : MonoBehaviour
{
    public enum ShakePreset { Bump, Explosion }

    [Header("Camera components")]
    [SerializeField] private CameraShaker _cameraShaker;
    [SerializeField] private PostProcessVolume _postProcessing;

    [Header("Camera shake")]
    [SerializeField] private CameraShakerSettings _shakeSettings;

    public void ShakeCamera(float roughness, float magnitude, float fadeIn, float fadeOut)
    {
        _cameraShaker.ShakeOnce(magnitude, roughness, fadeIn, fadeOut);
    }
    public void ShakeCamera(CameraShakerSettings settings)
    {
        _cameraShaker.ShakeOnce(settings.Magnitude, settings.Roughness, settings.FadeInTime, settings.FadeOutTime);
    }
    public void ShakeCamera(ShakePreset preset)
    {
        switch (preset)
        {
            case ShakePreset.Bump:
                _cameraShaker.Shake(CameraShakePresets.Bump);
                break;

            case ShakePreset.Explosion:
                _cameraShaker.Shake(CameraShakePresets.Explosion);
                break;
        }
    }
    public void ShakeCamera()
    {
        _cameraShaker.ShakeOnce(_shakeSettings.Magnitude,
                                _shakeSettings.Roughness,
                                _shakeSettings.FadeInTime,
                                _shakeSettings.FadeOutTime);
    }

    public void EnableDepthOFField()
    {
        if (!_postProcessing) return;

        CancelInvoke("DisableDepthOfField");
        {
            _postProcessing.profile.GetSetting<DepthOfField>().enabled.value = true;
        }
        Invoke("DisableDepthOfField", 0.3f);
    }
    public void DisableDepthOfField()
    {
        _postProcessing.profile.GetSetting<DepthOfField>().enabled.value = false;
    }
}