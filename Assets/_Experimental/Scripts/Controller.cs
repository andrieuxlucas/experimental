﻿/**
 *	Controller.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public abstract class Controller<CharacterType> : MonoBehaviour where CharacterType : Charater
{
    [Header("References")]
    [SerializeField] protected CharacterType _charater;

    private void Awake()
    {
        if (!_charater) _charater = GetComponent<CharacterType>();
    }

    private void Update()
    {
        if(_charater)
        {
            OnUpdate();
        }
    }

    public abstract float GetHorizontalInput();
    public abstract float GetVerticalInput();

    public abstract float GetCamHorizontalInput();
    public abstract float GetCamVerticalInput();

    public abstract bool GetActionInput();
    public abstract bool GetPunchInput();
    public abstract bool GetJumpInput();
    public abstract bool GetRunInput();

    protected virtual void OnUpdate()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            if(!_charater.StatusComponent.HasEffect<PermaUnmoveable>())
            {
                _charater.StatusComponent.ApplyEffect(ScriptableObject.CreateInstance<PermaUnmoveable>());
                _charater.StatusComponent.ApplyEffect(ScriptableObject.CreateInstance<PermaHeadLock>());
            }
            else
            {
                _charater.StatusComponent.RemoveEffect(ScriptableObject.CreateInstance<PermaUnmoveable>());
                _charater.StatusComponent.RemoveEffect(ScriptableObject.CreateInstance<PermaHeadLock>());
            }
        }

        if(_charater.CanMove)
            _charater.Move(GetHorizontalInput(), GetVerticalInput());

        if (_charater.CanTurn)
            _charater.TurnPerspective(GetCamHorizontalInput(), GetCamVerticalInput());

        if (_charater.CanRun)
        {
            if (GetRunInput())
                _charater.Run(GetHorizontalInput(), GetVerticalInput());

            else _charater.StopRun();
        }

        if (GetJumpInput())
            _charater.Jump();

        if (GetPunchInput())
            _charater.Punch();
    }
}