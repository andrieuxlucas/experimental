﻿/**
 *	BlinkPresset.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	14/08/2018	(dd/mm/yy)
 */

using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "BlinkPresset", menuName = "Experimental/BlinkPresset")]
public class BlinkPresset : ScriptableObject
{
    public float Duration { get { return _duration; } }
    public float Range { get { return _maxRange; } }

    public bool UseCurve { get { return _useCurve; } }

    [Header("Values")]
    [SerializeField] float _maxRange = 10.0f;
    [SerializeField] float _duration = 0.15f;
    [SerializeField] float _heightMultiplier;

    [Header("Settings")]
    [SerializeField] bool _useCurve;
    [SerializeField] AnimationCurve _curve;

    public void ClampCurve()
    {
        float __minValue = float.PositiveInfinity;
        float __maxValue = float.NegativeInfinity;

        foreach (var keyFrame in _curve.keys)
        {
            if (keyFrame.value > __maxValue)
                __maxValue = keyFrame.value;

            if (keyFrame.value < __minValue)
                __minValue = keyFrame.value;
        }

        float __delta = __maxValue - __minValue;

        List<Keyframe> __clampedKeyframes = new List<Keyframe>(_curve.length);

        foreach (var __keyFrame in _curve.keys)
            __clampedKeyframes.Add(new Keyframe(__keyFrame.time, (__keyFrame.value - __minValue) / __delta));

        _curve = new AnimationCurve(__clampedKeyframes.ToArray());
    }
    public float GetBlinkCurveValue(float time)
    {
        return _curve.Evaluate(time) * _heightMultiplier;
    }
    public Vector3 GetBlinkCurveHeightValue(float time)
    {
        return GetBlinkCurveValue(time) * Vector3.up;
    }
}