﻿/**
 *	BlinkTask.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	14/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public class BlinkTask : Task
{
    public BlinkTask(MonoBehaviour component) : base(component) { }
    public BlinkTask(MonoBehaviour component, Transform body, BlinkPresset settings) : this(component)
    {
        _bodyTransform = body;
        _settings = settings;

        _settings.ClampCurve();
    }

    [SerializeField] Transform _bodyTransform;
    [SerializeField] Vector3 _from, _to;

    [Header("Settings")]
    [SerializeField] BlinkPresset _settings;

    public void SetTargetPosition(Vector3 targetPosition)
    {
        _from = _bodyTransform.position;
        _to = targetPosition;
    }

    public override float GetDuration()
    {
        return _settings.Duration;
    }
    protected override void Update(float delta)
    {
        if(_settings.UseCurve)
        {
            Vector3 __arcOffset = _settings.GetBlinkCurveHeightValue(delta);
            _bodyTransform.position = Vector3.Lerp(_from, _to, delta) + __arcOffset;
        }
        else
        {
            _bodyTransform.position = Vector3.Lerp(_from, _to, delta);
        }
    }
}