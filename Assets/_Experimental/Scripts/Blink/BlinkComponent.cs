﻿/**
 *	BlinkComponent.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	11/08/2018	(dd/mm/yy)
 */

using System.Collections.Generic;
using UnityEngine;

public class BlinkComponent : DebuggableComponent
{
    public delegate void Action();

    public event Action OnBlinkFinish;
    public event Action OnBlinkBegin;
    public event Action OnBeginAim;

    //public Vector3 BlinkDirection { get }
    public BlinkState State { get { return _state; } }
    public bool IsBlinking { get { return _state.Equals(BlinkState.Blinking); } }

    [Header("Settings")]
    [SerializeField] private BlinkPresset _settings;
    [SerializeField] private float _momentumForce;

    [Header("References")]
    [SerializeField] private CharacterMovimentComponent _movimentComponent;
    [SerializeField] private CharacterController _characterController;
    [SerializeField] private CharacterPerspective _perspective;

    [Header("States")]
    [SerializeField] private BlinkState _state;

    [Header("VFX")]
    [SerializeField] private GameObject _blinkRepresentationTarget;
    [SerializeField] private GameObject _blinkMarkParticle;


    ScanParameters _scanParameters;

    BlinkTask _blinkTask;
    BlinkScan _blinkScan;
    GameObject blinkGroundParticle;
    GameObject _blinkReticleParticle;
    Queue<Vector3> _blinkDirectionalForces;

    private void Awake()
    {
        if (!_characterController) _characterController = GetComponent<CharacterController>();
        if (!_perspective) _perspective = GetComponent<PlayerPerspective>();

        _blinkDirectionalForces = new Queue<Vector3>();
        _blinkTask = new BlinkTask(this, transform, _settings);
        _blinkTask.BindOnTaskStart(InvokeOnBlinkBegin);
        _blinkTask.BindOnTaskEnd(InvokeOnBlinkEnd);

        _blinkScan = new BlinkScan(2, _characterController.height, _characterController.radius);
    }

    private void Start()
    {
        _scanParameters = new ScanParameters()
        {
            LayerMask = _perspective.GetScanableLayers(),
            Lenght = _settings.Range,

            ClimkberProjectionDepth = 0.25f
        };
    }

    private void Update()
    {
        if (_state.Equals(BlinkState.Aiming) && _perspective)
        {
            _scanParameters.Ray = _perspective.RayFromCameraToMouse;

            _blinkScan.Scan(_scanParameters);

            UpdateBlinkParticles();
        }
    }

    /// <summary>
    /// Starts the blink logic, setting the <see cref="State"/> to <see cref="BlinkState.Aiming"/>.
    /// </summary>
    public void PrepareBlink()
    {
        // FIX THIS
        if (_state.Equals(BlinkState.Blinking))
            _state = BlinkState.Idle;

        // If we're currently not in any blink state
        if(_state.Equals(BlinkState.Idle))
        {
            // Set the state to aiming
            _state = BlinkState.Aiming;

            blinkGroundParticle = InstantiateBlinkMarkParticle();
            _blinkReticleParticle = InstantiateBlinkRepresentationParticle();

            // Invokes the aiming event
            if (OnBeginAim != null)
                OnBeginAim();
        }
    }

    /// <summary>
    /// Finishes the blinking logic. setting the <see cref="State"/> to <see cref="BlinkState.Blinking"/>.
    /// </summary>
    public void ExecuteBlink()
    {
        // If we're currently aiming the blink cursor
        if(_state.Equals(BlinkState.Aiming))
        {
            _state = BlinkState.Blinking;

            Vector3 __targetBlinkPosition = _blinkScan.HitScanInfo.UpswingBlinkSuccessful ?
                    _blinkScan.HitScanInfo.ProjectionPoint :
                    _blinkScan.HitScanInfo.ReticlePosition;


            Vector3 __blinkDirection = (__targetBlinkPosition - transform.position).normalized;
            _blinkDirectionalForces.Enqueue(__blinkDirection * _momentumForce);

            _blinkTask.SetTargetPosition(__targetBlinkPosition);
            _blinkTask.StartTask();

            DestroyBlinkReticleParticles();
        }
    }

    public void InterruptBlink()
    {
        _state = BlinkState.Idle;

        DestroyBlinkReticleParticles();
    }


    private GameObject InstantiateBlinkRepresentationParticle()
    {
        if(_blinkRepresentationTarget)
            return Instantiate(_blinkRepresentationTarget, Vector3.zero, Quaternion.identity);

        return null;
    }

    private GameObject InstantiateBlinkMarkParticle()
    {
        if (_blinkMarkParticle)
            return Instantiate(_blinkMarkParticle, Vector3.zero, Quaternion.identity);

        return null;
    }

    private void DestroyBlinkReticleParticles()
    {
        if (blinkGroundParticle)
            Destroy(blinkGroundParticle);

        if (_blinkReticleParticle)
            Destroy(_blinkReticleParticle);
    }


    void UpdateBlinkParticles()
    {
        // Blink ground particle
        if (_blinkScan.HitScanInfo.GroundScanSuccessful)
        {
            if (!blinkGroundParticle.activeSelf)
                blinkGroundParticle.SetActive(true);

            blinkGroundParticle.transform.position = _blinkScan.HitScanInfo.GroundPosition;
        }
        else blinkGroundParticle.SetActive(false);

        // Blink reticle particle
        {
            if (!_blinkReticleParticle.activeSelf)
                _blinkReticleParticle.SetActive(true);

            if (_blinkScan.HitScanInfo.UpswingBlinkSuccessful)
                _blinkReticleParticle.transform.position = _blinkScan.HitScanInfo.ProjectionPoint;

            else
                _blinkReticleParticle.transform.position = _blinkScan.HitScanInfo.ReticlePosition;
        }
    }

    void InvokeOnBlinkBegin()
    {
        if (OnBlinkBegin != null)
            OnBlinkBegin();
    }

    void InvokeOnBlinkEnd()
    {
        _state = BlinkState.Idle;

        if (_movimentComponent)
            _movimentComponent.AddForce(_blinkDirectionalForces.Dequeue());

        if (OnBlinkFinish != null)
            OnBlinkFinish();
    }


    public override void Debug()
    {
        if (_blinkScan != null)
            if (_state.Equals(BlinkState.Aiming))
                _blinkScan.Debug();
    }
}