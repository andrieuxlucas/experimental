﻿/**
 *	BlinkScan.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	12/08/2018	(dd/mm/yy)
 */

using UnityEngine;

/// <summary>
/// Class used to create the projection needed to implement the <see cref="BlinkComponent"/>.
/// Sweeps an area trying to fit the player's <see cref="CapsuleProjection"/> at a given point.
/// </summary>
public class BlinkScan : IDrawableDebug
{
    #region Properties 

    public BlinkHitScan HitScanInfo { get; private set; }

    /// <summary>
    /// The max number of tryies this object will try to fit and
    /// realocate the player's <see cref="CapsuleProjection"/>.
    /// </summary>
    public ushort MaxScanTries { get; set; }

    #endregion Properties

    public BlinkScan(ushort maxProjectionTries, float characterHeight, float characterRadius)
    {
        MaxScanTries = maxProjectionTries;

        HitScanInfo = new BlinkHitScan(characterHeight, characterRadius);
    }

    public void Scan(ScanParameters parameters)
    {
        HitScanInfo.Update(parameters, MaxScanTries);
    }


    #region Auxiliar methods

    public void Debug()
    {
        if (!HitScanInfo.ReticleAndGroundAreTheSame)
        {
            DebugExtension.DrawPoint(HitScanInfo.GroundPosition, Color.blue);
            DebugExtension.DrawPoint(HitScanInfo.ReticlePosition, Color.yellow);
        }
        else DebugExtension.DrawPoint(HitScanInfo.ReticlePosition, Color.green);

        DebugExtension.DrawArrow(HitScanInfo.GetSurfaceHitUpwardsVector, Vector3.up, Color.red);

        if (HitScanInfo.UpswingBlinkConditionsMet)
        {
            if(HitScanInfo.UpswingBlinkSuccessful)
                HitScanInfo.CapsuleProjection.Debug(Color.green);
            else
                HitScanInfo.CapsuleProjection.Debug(Color.red);

            DebugExtension.DrawPoint(HitScanInfo.ProjectionPoint, Color.magenta);
        }
    }

    #endregion Auxiliar methods
}

public class BlinkHitScan
{
    /// <summary>
    /// The <see cref="RaycastHit"/> object of the initial raycast using <see cref="ScanParameters.Ray"/>.
    /// </summary>
    public RaycastHit SurfaceHit { get { return _surfaceHit; } }

    public RaycastHit GroundHit { get { return _groundHit; } }

    public Vector3 GetSurfaceHitUpwardsVector
    {
        get
        {
            if(SurfaceScanSuccessful)
            {
                Bounds __objectBounds = _surfaceHit.collider.bounds;

                _surfaceHitUpwardsVector = __objectBounds.center + Vector3.up * __objectBounds.extents.y;
            }

            return _surfaceHitUpwardsVector;
        }
    }
    public Vector3 ProjectionPoint
    {
        get
        {
            return CapsuleProjection.End;
        }
    }

    public CapsuleProjection CapsuleProjection { get; private set; }
    public Vector3 ReticlePosition { get; private set; }
    public Vector3 GroundPosition { get; private set; }

    public bool ReticleAndGroundAreTheSame { get { return ReticlePosition == GroundPosition; } }
    public bool UpswingBlinkConditionsMet { get; private set; }
    public bool UpswingBlinkSuccessful { get; private set; }

    /// <summary>
    /// If the (initial) raycast from <see cref="ScanParameters.Ray"/> was successful and hitted something.
    /// </summary>
    public bool SurfaceScanSuccessful { get; private set; }

    /// <summary>
    /// If the current scan found any mesh(ground) below the blink location.
    /// </summary>
    public bool GroundScanSuccessful { get; private set; }

    float _characterHeight, _characterRadius;
    Vector3 _surfaceHitUpwardsVector;
    RaycastHit _surfaceHit;
    RaycastHit _groundHit;

    public BlinkHitScan(float characterHeight, float characterRadius)
    {
        UpswingBlinkSuccessful = UpswingBlinkConditionsMet = SurfaceScanSuccessful = GroundScanSuccessful = false;

        _surfaceHitUpwardsVector = ReticlePosition = GroundPosition = Vector3.zero;

        _characterHeight = characterHeight;
        _characterRadius = characterRadius;
    }


    public void CreateCapsuleProjection(Vector3 start)
    {
        CapsuleProjection = new CapsuleProjection(start, _characterHeight, _characterRadius);
    }

    public void Update(ScanParameters parameters, ushort maxProjectionTries)
    {
        ResetState();

        // Creates the initial raycast from camera towards the mouse projection
        SurfaceScanSuccessful = Physics.Raycast(parameters.Ray, out _surfaceHit, parameters.Lenght, parameters.LayerMask);

        if(SurfaceScanSuccessful)
            ReticlePosition = SurfaceHit.point - (parameters.Ray.direction * _characterRadius);

        else
            ReticlePosition = parameters.Ray.GetPoint(parameters.Lenght);


        // Creates the secondary raycast from the ReticlePosition to the ground
        GroundScanSuccessful = RaycastToGround(out _groundHit, ReticlePosition + Vector3.up);

        if (GroundScanSuccessful)
            GroundPosition = _groundHit.point;

        if (ReticleAndGroundAreTheSame)
            ReticlePosition += Vector3.up * (_characterHeight / 2);


        // Upswing blink logic
        UpswingLogic(parameters, maxProjectionTries);
    }


    private void UpswingLogic(ScanParameters parameters, ushort maxProjectionTries)
    {
        if (SurfaceScanSuccessful && ScanHittedForwadFacingWall())
        {
            UpswingBlinkConditionsMet = true;

            Vector3 __directionTowardsPoint = parameters.Ray.direction;
            __directionTowardsPoint.y = 0.0f;

            CreateCapsuleProjection(SurfaceHit.point + (__directionTowardsPoint * parameters.ClimkberProjectionDepth));

            for (int i = 0; i <= maxProjectionTries; i++)
            {
                if (!CapsuleProjection.IsBeingOverlaped(parameters.LayerMask))
                {
                    if (Vector3.Distance(parameters.Ray.origin, CapsuleProjection.Center) <= parameters.Lenght)
                    {
                        RaycastToGround(out _groundHit, CapsuleProjection.End);
                        CreateCapsuleProjection(GroundHit.point);

                        if (CanCameraSeeProjection(parameters))
                        {
                            GroundPosition = GroundHit.point;
                            UpswingBlinkSuccessful = true;
                        }
                    }

                    break;
                }
                else
                {
                    RaycastToGround(out _groundHit, CapsuleProjection.End);
                    CreateCapsuleProjection(CapsuleProjection.Center);
                }
            }
        }
    }


    /// <summary>
    /// Creantes and <see cref="Physics.Raycast(Ray)"/> from the <paramref name="sourcePoint"/> towards
    /// the ground (downwards).
    /// </summary>
    /// <param name="hit">The <see cref="RaycastHit "/> variable where the hit information will be outputted.</param>
    /// <param name="sourcePoint">The point from where the <see cref="Ray"/> will be cast.</param>
    /// <returns></returns>
    private bool RaycastToGround(out RaycastHit hit, Vector3 sourcePoint)
    {
        return Physics.Raycast(new Ray(sourcePoint, Vector3.down), out hit, Mathf.Infinity);
    }

    private bool CanCameraSeeProjection(ScanParameters parameters)
    {
        Ray __raycast = new Ray(CapsuleProjection.End, (parameters.Ray.origin - CapsuleProjection.End).normalized);
        RaycastHit __hitInfo;

        Debug.DrawRay(__raycast.origin, __raycast.direction * 1000, Color.blue);
        if (Physics.Raycast(__raycast, out __hitInfo, Mathf.Infinity))
        {
            if (__hitInfo.transform.gameObject.layer == LayerMask.NameToLayer("PlayerObject"))
                return true;
        }

        return false;
    }

    private bool ScanHittedForwadFacingWall()
    {
        float __dotNormalAndUp = Vector3.Dot(SurfaceHit.normal, Vector3.up);
        //return (__dot < 0.25 && __dot > -0.25);

        float __angle = Mathf.Acos(__dotNormalAndUp) * Mathf.Rad2Deg;
        return __angle > 60 && __angle < 120;
    }

    private void ResetState()
    {
        UpswingBlinkConditionsMet = UpswingBlinkSuccessful = false;

        SurfaceScanSuccessful = GroundScanSuccessful = false;
    }
}