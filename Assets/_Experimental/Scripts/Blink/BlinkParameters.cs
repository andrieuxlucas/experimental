﻿/**
 *	BlinkParameters.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	12/08/2018	(dd/mm/yy)
 */

using UnityEngine;

public struct ScanParameters
{
    public Vector3 FarthestPoint { get { return Ray.GetPoint(Lenght); } }
    public LayerMask LayerMask { get; set; }
    public Ray Ray { get; set; }

    public float ClimkberProjectionDepth { get; set; }
    public float Lenght { get; set; }
}