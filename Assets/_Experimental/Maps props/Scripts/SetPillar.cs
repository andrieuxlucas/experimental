﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPillar : MonoBehaviour
{
    [ContextMenu("Ground")]
    public void Ground()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, Mathf.Infinity))
        {
            Vector3 scale = transform.localScale;
            transform.position = hit.point + Vector3.up * (scale.y / 2f);
        }
    }

    private void OnValidate()
    {
        Ground();
    }
}