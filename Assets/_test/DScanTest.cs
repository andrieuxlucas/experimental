﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

using Utitlities.ScanningMethods;


public class DScanTest : MonoBehaviour
{
    public PlayerPerspective perspective;
    public float distance = 100f;
    public float radius = 1.0f;
    public float speed = 10.0f;
    public int precision = 1;
    public ushort solverPrecision = 2;
    public float solverDepth = .5f;

    DirectionalScan dScan;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        
    }
    private void Update()
    {
        
        perspective.TurnView(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        if (Input.GetMouseButtonDown(0))
        {
            dScan = new DirectionalScan(new DirectionalConfig
            {
                Debug = true,
                DisableOnHit = true,
                InitialDirection = perspective.CameraForward,
                InitialPosition = perspective.CameraPosition,
                MaxDistance = distance,
                Precision = precision,
                ScanableLayers = perspective.GetScanableLayers(),
                ScanRadius = radius,
                Speed = speed
            });
            dScan.HitSolverPrecision = solverPrecision;
            dScan.HitSolverDepth = solverDepth;
        }

        if(dScan != null)
            dScan.Scan();
    }
}
