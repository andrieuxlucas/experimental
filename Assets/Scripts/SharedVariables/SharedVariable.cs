﻿/**
 *	SharedVariable.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	15/08/2018	(dd/mm/yy)
 */

using System;
using UnityEngine;

public class SharedVariable<T> : ScriptableObject
{
    public event Action<T> OnValueChanged;

    public static implicit operator T(SharedVariable<T> instance) { return instance.value; }
    public static implicit operator SharedVariable<T>(T value) { return new SharedVariable<T>() { value = value }; }

    private T value;

    public T Get() { return value; }
    public void Set(T newValue)
    {
        value = newValue;

        if (OnValueChanged != null)
            OnValueChanged(value);
    }
}