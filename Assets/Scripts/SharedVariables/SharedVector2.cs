﻿/**
 *	SharedVector2.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	15/08/2018	(dd/mm/yy)
 */

using UnityEngine;

[CreateAssetMenu(menuName = "SharedVariables/Vector2")]
public class SharedVector2 : SharedVariable<Vector2>
{
}