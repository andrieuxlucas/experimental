﻿/**
 *	ProceduralGeneration.cs
 *	Created by: Lucas Pazze Andrieux [andrieux.lucasp@gmail.com]
 *	Created on:	(dd/mm/yy)
 */

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class ProceduralGeneration : MonoBehaviour
{
    Transform PillarsParent => transform.Find("PillarsParent");
    Transform FloorsParent => transform.Find("FloorsParent");

    [Header("Material - Palette")]
    public Material[] colorPalette;

    [Header("Floors - Configuration")]
    [Range(0, 1000)] public ushort floorCount;
    public ushort floorSpaceDistribution = 100;
    public float floorMaxWidth = 20;
    public ushort floorMaxHeight = 20;
    public Vector2 floorSizeRange = new Vector2(10, 20);

    [Header("Pillars - Configuration")]
    [Range(0, 1000)] public ushort pillarCount;
    public ushort pillarSpaceDistribution = 100;
    public ushort pillarMaxHeight = 20;
    public ushort pillarMaxRotation = 90;
    public Vector2 pillarSizeRange = new Vector2(10, 20);

    List<GameObject> blocks;
    GameObject sampleCube;

    [ContextMenu("Generate")]
    public void Generate()
    {
        if (blocks == null)
            blocks = new List<GameObject>();

        else Clear();

        sampleCube = GameObject.CreatePrimitive(PrimitiveType.Cube);

        blocks.AddRange(GenerateFloors());
        blocks.AddRange(GeneratePillars());

        DestroyImmediate(sampleCube);
    }

    [ContextMenu("Clear")]
    public void Clear()
    {
        Transform floorsParent = FloorsParent;
        Transform pillarsParent = PillarsParent;

        int floorCount = floorsParent.childCount;
        for (int i = 0; i < floorCount; i++)
            DestroyImmediate(floorsParent.GetChild(0).gameObject);

        int pillarCount = PillarsParent.childCount;
        for (int i = 0; i < pillarCount; i++)
            DestroyImmediate(pillarsParent.GetChild(0).gameObject);

        blocks.Clear();
    }

    IEnumerable<GameObject> GenerateFloors()
    {
        Transform parent = FloorsParent;
        GameObject[] floorArray = new GameObject[floorCount];
        for (int i = 0; i < floorCount; i++)
        {
            floorArray[i] = Instantiate(sampleCube);

            Vector3 position = Random.insideUnitSphere * floorSpaceDistribution;
            position.y = i * (floorMaxHeight / (floorCount * 1.0f));
            floorArray[i].transform.position = position;

            Vector3 scale = new Vector3(Random.Range(floorSizeRange.x, floorSizeRange.y), Random.value * floorMaxWidth, Random.Range(floorSizeRange.x, floorSizeRange.y));
            floorArray[i].transform.localScale = scale;

            if (colorPalette.Length > 0)
                floorArray[i].GetComponent<MeshRenderer>().material = colorPalette[Random.Range(0, colorPalette.Length)];

            floorArray[i].transform.SetParent(parent);
        }

        return floorArray;
    }
    IEnumerable<GameObject> GeneratePillars()
    {
        Transform parent = PillarsParent;
        GameObject[] pillarArray = new GameObject[pillarCount];
        for (int i = 0; i < pillarCount; i++)
        {
            pillarArray[i] = Instantiate(sampleCube);

            Vector3 scale = new Vector3(Random.Range(pillarSizeRange.x, pillarSizeRange.y), Random.value * pillarMaxHeight, Random.Range(pillarSizeRange.x, pillarSizeRange.y));
            pillarArray[i].transform.localScale = scale;

            pillarArray[i].transform.rotation = Quaternion.Euler(Random.value * pillarMaxRotation, 0, Random.value * pillarMaxRotation);

            Vector3 position = Random.insideUnitSphere * pillarSpaceDistribution;
            //position = blocks[Random.Range(0, blocks.Count)].transform.position;
            position += Vector3.up * pillarArray[i].transform.localScale.y;
            pillarArray[i].transform.position = position;

            if(colorPalette.Length > 0)
                pillarArray[i].GetComponent<MeshRenderer>().material = colorPalette[Random.Range(0, colorPalette.Length)];

            pillarArray[i].transform.SetParent(parent);
        }

        return pillarArray;
    }
}